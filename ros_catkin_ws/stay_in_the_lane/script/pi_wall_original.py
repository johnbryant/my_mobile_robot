#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
import move

direction = +1
wallDistance = 0.8
e = 0
maxSpeed =1.2
P = 2
D = 0.3
angleCoef= 0.1
mov = move.movement()
bound_angular = 0.5
bound_linear = 0.5
PI = 3.141592
class stay_in_the_lane:
	def __init__(self):

		rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=1)
		rospy.sleep(0)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
		rospy.spin()
	def callback (self,msg):
		Ranges=msg.ranges
		angle_increment=msg.angle_increment
		size= len (msg.ranges)
		minIndex = size*(direction+1)/4;
  		maxIndex = size*(direction+3)/4;
		for i in range (minIndex,maxIndex):
			if Ranges[i]<Ranges[minIndex] and Ranges[i]>0:
				minIndex=i

		angleMin = (minIndex-size/2)*angle_increment	
		#print angleMin
		distMin=Ranges[minIndex]
		distFront= Ranges[size/2]
		global e
		diffE = (distMin - wallDistance) - e
		e = distMin - wallDistance
		
		self.publishMessage(diffE,angleMin,e,distFront)

	def publishMessage(self,diffE,angleMin,e,distFront):
		msg=Twist()
		#print (" direction*(P*e + D*diffE)", direction*(P*e + D*diffE))
		#print (" angleCoef * (angleMin - PI*direction/2)", angleCoef * (angleMin - PI*direction/2))
		msg.angular.z = -direction*(P*e + D*diffE) + angleCoef * (angleMin - PI*direction/2)
 
		if msg.angular.z > bound_angular:
			msg.angular.z = bound_angular
		elif msg.angular.z < -bound_angular : 
			msg.angular.z = -bound_angular  

		if (distFront < wallDistance):
			msg.linear.x = 0
		elif (distFront < wallDistance * 2):
			msg.linear.x = 0.4*maxSpeed
			#msg.linear.x = 0.4*mov.move_forward()

		elif (abs(angleMin)>1.75):
			 msg.linear.x = 0.5*maxSpeed
 			 #msg.linear.x = 0.5*mov.move_forward()

		else :
			 msg.linear.x = maxSpeed
		
		if msg.linear.x > bound_linear:
			msg.linear.x = bound_linear
		elif msg.linear.x < -bound_linear : 
			msg.linear.x = -bound_linear

		self.pub_vel.publish(msg)

def main():
    node = stay_in_the_lane()

if __name__ == '__main__':
    main()
 
