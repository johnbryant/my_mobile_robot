#!/usr/bin/env python

import roslib
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist,Vector3
import random
import math
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion
from geometry_msgs.msg import Pose, PoseArray
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
import numpy
from visualization_msgs.msg import Marker
import matplotlib
import matplotlib.pyplot as plt
import numpy.matlib
import sys 
angle_increment = -0.00436940183863
#angle_increment = 0.00872664619237 #angle_increment for actual robot
#angle_min = -2.35619449615 #angle_min for actual robot 
angle_min = 1.57079994678


class Ransac(object):
	def __init__(self):
		# init the node
        	rospy.init_node('Ransac', anonymous=False)
	        self.pub_obj1 = rospy.Publisher("/points",Marker,queue_size=1)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
	        rospy.spin()

	def calc_distance(self,random_point_cartesian_array,points1_transposed,new_number):
		kLine = numpy.array([[(random_point_cartesian_array[0][1] - random_point_cartesian_array[0][0])],[(random_point_cartesian_array[1][1] - random_point_cartesian_array[1][0])]]) # two points relative distance	
		
	
		kLineNorm = kLine/(numpy.linalg.norm(kLine)) 
		normVector = numpy.array([-kLineNorm[1],kLineNorm[0]])# Ax+By+C=0 A=-kLineNorm(2),B=kLineNorm(1)
		normVector = numpy.transpose(normVector)

		temp_sample= numpy.array([random_point_cartesian_array[:,0]])
		temp = numpy.transpose(temp_sample)
		distance = normVector.dot(points1_transposed - numpy.matlib.repmat(temp,1,new_number))			
		return distance 

	def calc_param(self,random_point_cartesian_array):
		parameter1 = (random_point_cartesian_array[1][1]-random_point_cartesian_array[1][0])/(random_point_cartesian_array[0][1]-random_point_cartesian_array[0][0])

		parameter2 = random_point_cartesian_array[1][0] -parameter1*random_point_cartesian_array[0][0]

		bestParameter1=parameter1
		bestParameter2=parameter2
		return bestParameter1, bestParameter2

	def cartesian_to_polar(self,x1_coordinate,y1_coordinate,x2_coordinate,y2_coordinate):
		r1 = math.sqrt(math.pow(x1_coordinate,2) + math.pow(y1_coordinate,2))
		theta1 = numpy.arctan(y1_coordinate/x1_coordinate)
		r2 = math.sqrt(math.pow(x2_coordinate,2) + math.pow(y2_coordinate,2))
		theta2 = numpy.arctan(y2_coordinate/x2_coordinate)
		r = math.sqrt(math.pow(r1,2) + math.pow(r2,2))
		theta = abs(theta1-theta2)
		return r,theta




	def ransac(self,msg,pub_obj1): 


	    #num = 2 
	    threshDist = 0.1
	    inlierRatio = 0.01
	    iter = 1000
	    
	    Ranges = msg
	    number = len(Ranges)# Total number of points
	    #angle_min = Ranges.angle_min
	    #angle_increment = Ranges.angle_increment

	    bestInNum = 0 #Best fitting line with largest number of inliers

	    bestParameter1=0
	    bestParameter2=0 # parameters for best fitting line
	    #print "Ranges[0]"
	    #print Ranges[0]
	    counter = 0
	    points = []
	    for r in Ranges:
		counter+=1
		if (r < 2): # distance smaller than 3 meters 
			x_coordinate = r * math.cos(angle_min + (angle_increment * counter) ) 
			y_coordinate = r * math.sin(angle_min + (angle_increment * counter) )
			z_coordinate = 0
			points.append((x_coordinate,y_coordinate))
	    points1= numpy.asarray(points)
	    new_number = points1.shape[0]

	    points1_transposed = numpy.transpose(points1)
	    lists = []
	    for i in range(0,iter):
		    # Randomly select 2 points
			temp_points1 = numpy.transpose(points1_transposed) 
			random_points_cartesian = random.sample(temp_points1,2)
			random_point_cartesian_array = numpy.array([[random_points_cartesian[0][0],random_points_cartesian[1][0]],[random_points_cartesian[0][1],random_points_cartesian[1][1]]])
		    # Compute the distances between all points with the fitting line 
			distance = self.calc_distance(random_point_cartesian_array,points1_transposed,new_number)
	 
		        lists = []
	   		for k in range(0,distance.shape[1]-1):

				if distance[0,k]<=threshDist:
					lists.append(k)	
			#print lists
			temp1=numpy.asarray(lists)
			inlierIdx = temp1
		    # Compute the inliers with distances smaller than the threshold
			inlierNum = inlierIdx.shape[0]
			#print inlierNum
		    # Update the number of inliers and fitting model if better model is found     
		    	if (inlierNum>=round(inlierRatio*new_number)) and (inlierNum>bestInNum) :
		    	    bestInNum = inlierNum;
		  	    bestParameter1,bestParameter2 =self.calc_param(random_point_cartesian_array)


		    	
	    #xAxis = numpy.linspace(-0.5,3,1000)
	    #xAxis = points1[:,0]
	    #yAxis = bestParameter1*xAxis + bestParameter2
	    #y_temp = numpy.transpose(points1)
	    #plt.plot(xAxis,yAxis,'r')
	    #plt.plot(points1[:,0], points1[:,1],'o')
	    #plt.show()
	    #raw_input ("shit")
	    #print points1_transposed.shape
	    #print points1_transposed[:,1]
	    #print inlierIdx[1]
	    index2 = max(inlierIdx)
	    #print index2
	    index1 = min(inlierIdx)
	    #print index1
	    #print random_point_cartesian_array.shape
	    #construct a marker between the farthest points in the inlier list
	    #line_marker=Marker()
	    #line_marker.header.frame_id = "laser_link"
	    #line_marker.type = Marker.LINE_STRIP
	    #line_marker.color.g = 1
	    #line_marker.color.a = 1
	    #line_marker.type = Marker.ADD
	    #line_marker.lifetime = rospy.Duration(0)
	    #line_marker.scale.x = 0.1
	    best_random_point1 = points1_transposed[:,index1]
	    best_random_point2 = points1_transposed[:,index2]
	    #print best_random_point1
	    #print best_random_point2
	    #p1 = Point()
	    #p1.x = best_random_point1[0]
	    #p1.y = bestParameter1*best_random_point1[0] + bestParameter2
	    #p1.z = 0
	    #p2 = Point()
	    #p1.x = best_random_point2[0]
	    #p1.y = bestParameter1*best_random_point2[0] + bestParameter2
	    #p1.z = 0
	    #line_marker.points.append(p1)
	    #line_marker.points.append(p2)
	    #pub_obj1.publish(line_marker)
	
	    marker = Marker()
	    marker.header.frame_id = "laser_link"
	    marker.type = marker.LINE_STRIP
	    marker.action = marker.ADD

	    # marker scale
	    marker.scale.x = 0.15
	    marker.scale.y = 0
	    marker.scale.z = 0
	    #50,205,50
	    # marker color
	    marker.color.a = 1.0
	    marker.color.r = 128.0
	    marker.color.g = 128.0
	    marker.color.b = 0.0

	    # marker orientaiton
	    marker.pose.orientation.x = 0.0
	    marker.pose.orientation.y = 0.0
	    marker.pose.orientation.z = 0.0
	    marker.pose.orientation.w = 0.0

	    # marker position
	    marker.pose.position.x = 0.0
	    marker.pose.position.y = 0.0
	    marker.pose.position.z = 0.0

	    # marker line points
	    marker.points = []
	    # first point
	    first_line_point = Point()
	    first_line_point.x = best_random_point1[0]
	    first_line_point.y = bestParameter1*best_random_point1[0] + bestParameter2
	    first_line_point.z = 0.0
	    marker.points.append(first_line_point)
	    # second point
	    second_line_point = Point()
	    second_line_point.x = best_random_point2[0]
	    second_line_point.y = bestParameter1*best_random_point2[0] + bestParameter2
	    second_line_point.z = 0.0
	    marker.points.append(second_line_point)


	    r_polar,theta_polar = self.cartesian_to_polar(first_line_point.x,first_line_point.y,second_line_point.x,second_line_point.y)
	    #print "r"
	    #print r_polar
	    #print "theta"
	    #print theta_polar


	    # Publish the Marker
	    self.pub_obj1.publish(marker)

	    #rospy.sleep(0.5)
	    print r_polar
	    print "r_polar"
	    print theta_polar
	    print "theta_polar"
	    return r_polar,theta_polar

	def callback (self,msg):
		self.ransac(msg.ranges,self.pub_obj1)	
	   
   

def main():

   node = Ransac()

if __name__ == '__main__':
    main()
 
