#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
import move
from laser_line_extraction.msg import LineSegmentList
direction = -1
wallDistance = 1
e=0
maxSpeed =0.9
P = 1
D = 0.3
angleCoef= 0.1
mov = move.movement()

PI = 3.141592
class stay_in_the_lane:
	def __init__(self):

		rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=1)
		rospy.sleep(0)
		self.sub = rospy.Subscriber('/line_segments', LineSegmentList, self.callback, queue_size = 1)
		rospy.spin()
	def callback (self,msg):
		print msg.line_segments
		distMin = msg.line_segments[0].radius
		#print "distMin"
		#print distMin
		angleMin = msg.line_segments[0].angle
		#print "angleMin"
		#print angleMin

		global e


		diffE = (distMin - wallDistance) - e
		e = distMin - wallDistance
		print "error"
		print e
		self.publishMessage(diffE,angleMin,e)

	def publishMessage(self,diffE,angleMin,e):
		msg=Twist()
		msg.angular.z = direction*(P*e + D*diffE) + angleCoef * (angleMin - PI*direction/2)
		#print "angular velo"
		#print msg.angular.z
		if msg.angular.z >2 :
			msg.angular.z = 2 
		else :
			msg.linear.x = 0.5*mov.move_forward()
		#if (distFront < wallDistance):
		#	msg.linear.x = 0
		#elif (distFront < wallDistance * 2):
			#msg.linear.x = 0.4*maxSpeed
		#	msg.linear.x = 0.4*mov.move_forward()

		#elif (abs(angleMin)>1.75):
			 #msg.linear.x = 0.5*maxSpeed
 		#	 msg.linear.x = 0.5*mov.move_forward()

		#else :
		#	 msg.linear.x = mov.move_forward()

		self.pub_vel.publish(msg)

def main():
    node = stay_in_the_lane()

if __name__ == '__main__':
    main()
 
