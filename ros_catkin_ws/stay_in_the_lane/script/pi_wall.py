#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
import move
import ransac_markers_class 

direction = -1
wallDistance = 1
e=0
maxSpeed =0.9
P = 1
D = 0.3
angleCoef= 0.1
mov = move.movement()
rans = ransac_markers_class.Ransac()

PI = 3.141592
class stay_in_the_lane:
	def __init__(self):

		rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=1)
		rospy.sleep(0)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
		rospy.spin()
	def callback (self,msg):
		#self.ransac(msg.ranges,self.pub_obj1)	
		Ranges=msg.ranges
		angle_increment=msg.angle_increment
		size= len (msg.ranges)
		minIndex = size*(direction+1)/4;
  		maxIndex = size*(direction+3)/4;
		#print "minIndexb4"
		#print minIndex
		#print "range 0"
		#print Ranges[719]
		for i in range (minIndex,maxIndex):
			if Ranges[i]<Ranges[minIndex] and Ranges[i]>0:
				minIndex=i

		#print "minIndexafter"
		#print minIndex
		angleMin = (minIndex-size/2)*angle_increment	
		#print angleMin
		distMin=Ranges[minIndex]
		distFront= Ranges[size/2]
		global e
		#print "error"
		#print e


		distMin,angleMin = rans.ransac(msg.ranges,self.pub_vel)
		print "distMin"
		print distMin
		print "angleMin"
		print angleMin


		diffE = (distMin - wallDistance) - e
		e = distMin - wallDistance
		
		self.publishMessage(diffE,angleMin,e,distFront)

	def publishMessage(self,diffE,angleMin,e,distFront):
		msg=Twist()
		msg.angular.z = direction*(P*e + D*diffE) + angleCoef * (angleMin - PI*direction/2)
		print "angular velo"
		print msg.angular.z
		if msg.angular.z >2 :
			msg.angular.z = 2 
		if (distFront < wallDistance):
			msg.linear.x = 0
		elif (distFront < wallDistance * 2):
			#msg.linear.x = 0.4*maxSpeed
			msg.linear.x = 0.4*mov.move_forward()

		elif (abs(angleMin)>1.75):
			 #msg.linear.x = 0.5*maxSpeed
 			 msg.linear.x = 0.5*mov.move_forward()

		else :
			 msg.linear.x = mov.move_forward()

		self.pub_vel.publish(msg)

def main():
    node = stay_in_the_lane()

if __name__ == '__main__':
    main()
 
