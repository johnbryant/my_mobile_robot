#! /usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
import sys
PI = 3.1415926535897

class movement:
	def __init__(self):
	       # rospy.init_node('move_robot_node', disable_signals=True)   	   
        	self.cmd_vel_pub = rospy.Publisher('/marvin/diff_drive_controller/cmd_vel', Twist, queue_size=10)
		rospy.sleep(1)
	def move_forward(self):
			cmd = Twist()
			cmd.linear.x = 0.8 #Move the robot with a linear velocity in the x axis
			cmd.angular.z = 0.0 #Move the with an angular velocity in the z axis
			self.cmd_vel_pub.publish(cmd)
			return cmd.linear.x
	def stop(self):
			cmd = Twist()
			cmd.linear.x = 0.0 #Move the robot with a linear velocity in the x axis
			cmd.angular.z = 0.0 #Move the with an angular velocity in the z axis
			self.cmd_vel_pub.publish(cmd)
	def move_backward(self):
			cmd = Twist()
			cmd.linear.x = -1.5 #Move the robot with a linear velocity in the x axis
			cmd.angular.z = 0.0 #Move the with an angular velocity in the z axis
			self.cmd_vel_pub.publish(cmd)			
	def rotate_right_90(self):
	    		cmd = Twist()
			# Receiveing the user's input
			print("Let's rotate your robot 90 degrees right")
			#speed = input("Input your speed (degrees/sec):")
			speed = 100
			#angle = input("Type your distance (degrees):")
			angle = 90
			#clockwise = input("Clockwise?: ") #True or false
			clockwise = True

			#Converting from angles to radians
			angular_speed = speed*2*PI/360
			relative_angle = angle*2*PI/360

			#We wont use linear components
			cmd.linear.x=0
			cmd.linear.y=0
			cmd.linear.z=0
			cmd.angular.x = 0
			cmd.angular.y = 0

			# Checking if our movement is CW or CCW
			if clockwise:
				cmd.angular.z = -abs(angular_speed)
			else:
				cmd.angular.z = abs(angular_speed)
			# Setting the current time for distance calculus
			t0 = rospy.Time.now().to_sec()
			current_angle = 0

			while(current_angle < relative_angle):
				self.cmd_vel_pub.publish(cmd)
				t1 = rospy.Time.now().to_sec()
				current_angle = angular_speed*(t1-t0)
		
			#Forcing our robot to stop
			cmd.angular.z = 0
			self.cmd_vel_pub.publish(cmd)
			return cmd.angular.z
	def rotate_left_90(self):
	    		cmd = Twist()
			# Receiveing the user's input
			print("Let's rotate your robot 90 degrees left")
			#speed = input("Input your speed (degrees/sec):")
			speed = 100
			#angle = input("Type your distance (degrees):")
			angle = 90
			#clockwise = input("Clockwise?: ") #True or false
			clockwise = False

			#Converting from angles to radians
			angular_speed = speed*2*PI/360
			relative_angle = angle*2*PI/360

			#We wont use linear components
			cmd.linear.x=0
			cmd.linear.y=0
			cmd.linear.z=0
			cmd.angular.x = 0
			cmd.angular.y = 0

			# Checking if our movement is CW or CCW
			if clockwise:
				cmd.angular.z = -abs(angular_speed)
			else:
				cmd.angular.z = abs(angular_speed)
			# Setting the current time for distance calculus
			t0 = rospy.Time.now().to_sec()
			current_angle = 0

			while(current_angle < relative_angle):
				self.cmd_vel_pub.publish(cmd)
				t1 = rospy.Time.now().to_sec()
				current_angle = angular_speed*(t1-t0)
		
			#Forcing our robot to stop
			cmd.angular.z = 0
			self.cmd_vel_pub.publish(cmd)
	def rotate_180(self):
	    		cmd = Twist()
			# Receiveing the user's input
			print("Let's rotate your robot 180 degrees left")
			#speed = input("Input your speed (degrees/sec):")
			speed = 100
			#angle = input("Type your distance (degrees):")
			angle = 180
			#clockwise = input("Clockwise?: ") #True or false
			clockwise = False

			#Converting from angles to radians
			angular_speed = speed*2*PI/360
			relative_angle = angle*2*PI/360

			#We wont use linear components
			cmd.linear.x=0
			cmd.linear.y=0
			cmd.linear.z=0
			cmd.angular.x = 0
			cmd.angular.y = 0

			# Checking if our movement is CW or CCW
			if clockwise:
				cmd.angular.z = -abs(angular_speed)
			else:
				cmd.angular.z = abs(angular_speed)
			# Setting the current time for distance calculus
			t0 = rospy.Time.now().to_sec()
			current_angle = 0

			while(current_angle < relative_angle):
				self.cmd_vel_pub.publish(cmd)
				t1 = rospy.Time.now().to_sec()
				current_angle = angular_speed*(t1-t0)
		
			#Forcing our robot to stop
			cmd.angular.z = 0
			self.cmd_vel_pub.publish(cmd)


#while not rospy.is_shutdown():
#	mov = movement()
#	mov.move_forward()
#	rospy.sleep(1)
#	mov.rotate_right_90()
#	rospy.sleep(1)
#	mov.stop()
#	rospy.sleep(1)
#	mov.move_backward()
#	rospy.sleep(1)
#	mov.rotate_180()

