#!/usr/bin/env python

import rospy
import math
import numpy
from sensor_msgs.msg import LaserScan
import struct 
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker

GAP_SIZE = 100
MAX_RANGE = 4
MIN_RANGE = 0.75 
MAX_SMALLER_PERCENT = 0.4
MAX_RANGE_STEP = 0.1

global Ranges

class sensing:
	def __init__(self):
	        rospy.init_node('sense_robot_node', anonymous=True)   	   
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
	    	self.pub_obj1 = rospy.Publisher("/marker_basic",Marker,queue_size=1)	    	
		rospy.spin()
	def callback (self,msg):
		global angle_min
		global angle_increment
		global range_min
		Ranges = msg.ranges
		angle_min = msg.angle_min
		angle_increment = msg.angle_increment
		range_min = msg.range_min
		ranges_array =  numpy.asarray(Ranges)
		self.test_sensing(ranges_array)

	def countLessRange(self,ranges_array,range1):
	#Counts the amount of lasers that have a lower distance than "range"
		count = 0
		for i in range(0,ranges_array.shape[0]):
			if ranges_array[i] < range1:
					count = count +1
		return count

	def determineMaxRange(self, ranges):
	#Keeps decreasing the maximum detection range, until the percentage condition is met. The varying range detection part.

		if (MAX_RANGE_STEP <= 0):
        		print("Sensing in undefined state! Deadlock, stepsize <= 0 for maxRange finding!");
    		if (MAX_SMALLER_PERCENT > 1):
        		print("Sensing in undefined state! maxSmallerPercent > 1!");
   		if (MAX_SMALLER_PERCENT < 0):
        		print("Sensing in undefined state! maxSmallerPercent < 0!");
		maxRange = MAX_RANGE
		
		while (maxRange  > MIN_RANGE) :
			if (self.countLessRange(ranges, maxRange) <= MAX_SMALLER_PERCENT *ranges.shape[0]):
			 	maxRange = maxRange - MAX_RANGE_STEP
	 		else:
	 			break
		return maxRange

	def clipRanges(self, ranges_array, minRange, maxRange):
	#Sets the laser-distances that are larger than the maxi2mum range equal to the maximum range.
	#Including the very high distances (<minrange).
		for i in range(0,ranges_array.shape[0]):
			if ranges_array[i] < minRange or ranges_array[i] >maxRange :
				#print "mphkes"
				ranges_array[i] = maxRange

		return ranges_array
	def calculateXY_1_beam(self, ranges, angle_min, angle_increment, ray):
	#Returns the x and y-coordinates of a specific ray
  		coordinates_list = []
		theta = angle_min + angle_increment * ray
		x_coordinate = math.cos((theta) * ranges[ray]) 
		y_coordinate = math.sin((theta) * ranges[ray])

		return x_coordinate, y_coordinate

	def calculateXY_many_beams(self,ranges_array, angle_min, angle_increment):
	#return the x and y coordinates of all beams 
		coordinates_list_all_beams = []
		for ray in range(0,ranges_array.shape[0]): 
			#print ray 
			x,y = self.calculateXY_1_beam(ranges_array,angle_min,angle_increment,ray)	
			coordinates_list_all_beams.append((x,y))	

		coordinates_array_all_beams = numpy.asarray(coordinates_list_all_beams)
		return coordinates_array_all_beams

	def findEdgeRays(self, ranges_array, maxRange):
	#returns the edge rays
		openCount = 0 
		right = 0 
		left = -1 
		EdgeRay = [right, left]
		EdgeRays = []
		for i in range(1,ranges_array.shape[0]):
			#print i
			if ranges_array[i] == maxRange and ranges_array[i-1] != maxRange: #if it's a potential gap 
       				#print "we found our right edge!"										#AND the previous ray was a wall
				EdgeRay[0] = i -1
			if (ranges_array[i]!= maxRange or  i == ranges_array.shape[0] - 1 ) and (ranges_array[i - 1] == maxRange):
				#if it's a wall            OR   the last ray                    AND  the previous ray was a potential gap
				EdgeRay[1] = i
				#print " We found our left edge!"
				if (EdgeRay[1] < 0 or EdgeRay[0] < 0):
					print "Sensing in undefined state! Found a left edge without a right edge at ray"
				else: 
					EdgeRays.append((EdgeRay[0],EdgeRay[1]))
					#print "found edge-ray pair"
					EdgeRay[0] = -1
					EdgeRay[1] = -1
		#print("EdgeRays",EdgeRays)
		EdgeRays_array = numpy.asarray(EdgeRays)
		return EdgeRays_array

	def compute_edgeRays(self,ranges_array):
		maxRange = self.determineMaxRange(ranges_array)
		#print("maxRange",maxRange)
		clippedRanges = self.clipRanges(ranges_array, range_min, maxRange)
		#print ("clippedRanges", clippedRanges)
		edgeRays = self.findEdgeRays(clippedRanges, maxRange)
		#print ("edgeRays", edgeRays)
		return edgeRays

	
	def findEdgePoints(self,ranges_array):
	#returns the x and y points of the of the edge beams
		edgeRays = self.compute_edgeRays(ranges_array)
		maxRange = self.determineMaxRange(ranges_array)
		clippedRanges = self.clipRanges(ranges_array, range_min, maxRange)
		edgePoints = []
		for i in range(0,edgeRays.shape[0]):
			#leftPoint = self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,min(edgeRays[0]))#edgeRays[ii].left)
			leftPoint = self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,edgeRays[i][1])#edgeRays[ii].left)
			rightPoint =  self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,edgeRays[i][0])#edgeRays[ii].right)			
			edgePoints.append(rightPoint)
			edgePoints.append(leftPoint)

		edgePoints_array = numpy.asarray(edgePoints)
		return edgePoints_array

	def findGapRays(self, edgeRays, rangesSize, openSpacePoints):
	#Returns which laser points are in a gap or not, only if the gap is big enough for Pico (=corridor).

		isGapRays = []
		for m in range(0,rangesSize):
			isGapRays.append(False)
		
		for i in range(0,edgeRays.shape[0]):
			if (edgeRays[i][1] - edgeRays[i][0]) > openSpacePoints:

				for j in range((edgeRays[i][0]+1),edgeRays[i][1]):

					isGapRays[j] = True

		if isGapRays[0] == 1 :
			#print "Sensing in undefined state! First ray is a gap!" 
			print("")
		if len(isGapRays) > 0:
			if isGapRays[len(isGapRays) - 1] == 1 : 
				#print "Sensing in undefined state! Last ray is a gap!"
				print("")

		else :
			print "Sensing error"
		return isGapRays


	def computeGapRays(self, ranges_array, openSpacePoints):
		edgeRays = self.compute_edgeRays(ranges_array)
		gapRays = self.findGapRays(edgeRays, ranges_array.shape[0], openSpacePoints)
		return gapRays


	def countGaps(self, isGapRays):
	#returns the number of gaps 
		flips1 = 0
		isGapRays = numpy.asarray(isGapRays)
		for i in range(1,len(isGapRays)):
			if (isGapRays[i-1] != isGapRays[i]):
				flips1 = flips1 + 1
		gaps = flips1/2
		return gaps

	






	def test_sensing(self, ranges_array):
		isGapRays = self.computeGapRays(ranges_array, GAP_SIZE)
		edgeRays = self.compute_edgeRays(ranges_array)
		print ("edgeRays",edgeRays)
		for i in range(0,edgeRays.shape[0]):
		    if edgeRays[i][1] < ranges_array.shape[0]/4 : 
			print("gap left")
		    elif edgeRays[i][1] > ranges_array.shape[0]/4 and edgeRays[i][1] < ranges_array.shape[0]*3/4 :
			print("gap front")
		    elif edgeRays[i][1] > ranges_array.shape[0]*3/4 and edgeRays[i][1] < ranges_array.shape[0] : 
			print("gap right")
		    
		numGaps = self.countGaps(isGapRays)
		print ("posa gaps exeis re malaka",numGaps)
    		
    		return numGaps


def main():
    """ main function
    """
    node = sensing()

if __name__ == '__main__':
    while not rospy.is_shutdown() :
	    main()
 
