#!/usr/bin/env python

import rospy
import math
from sensor_msgs.msg import LaserScan
import numpy

GAP_SIZE = 100
MAX_RANGE = 5
MIN_RANGE = 0.75 
MAX_SMALLER_PERCENT = 0.4
MAX_RANGE_STEP = 0.1


class sensing:
	def __init__(self):
	        rospy.init_node('sense_robot_node', anonymous=True)   	   
        	#self.pub_move = rospy.Publisher('/marvin/diff_drive_controller/cmd_vel', Twist, queue_size=10)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)

	def callback (self,msg): #ilimited detection circle 
		global Ranges
		Ranges = msg.ranges
		#print self.Ranges
		coordinates = []
		
		count = 0 
		max_Range = MAX_RANGE
		angle_min = msg.angle_min
		angle_increment = msg.angle_increment
		ranges_array =  numpy.asarray(Ranges)

		vecsize = ranges_array.shape[0]
		refdata = numpy.empty(vecsize)
		#print refdata.shape 
		#this part counts all the values that are smaller than the initial max range.
		for i in range(0,ranges_array.shape[0]):
			if ranges_array[i] < max_Range:
					count = count +1
		#if 60% or more of all values are smaller than the initial maxrange, the max range is reduced.
		if (count > 600):  
			 	maxRange = 1.1

		counter = 0
		
		x_coordinate = []
		y_coordinate = []
		coordinates
		anglerad = 0
		for i in range(0,ranges_array.shape[0]):	
			if ranges_array[i]> max_Range:
				ranges_array[i] = max_Range
			elif ranges_array[i] < 0.01:
				ranges_array[i] = max_Range

		#x and y values are calculated for each element in the ranges vector
			theta = angle_min + angle_increment * i
			x_coordinate = math.cos((theta) * ranges_array[i]) 
			y_coordinate = math.sin((theta) * ranges_array[i])
			theta = theta - angle_increment
			coordinates.append((x_coordinate,y_coordinate))
		#print len(coordinates)	
		coordinates = numpy.asarray(coordinates)
		#print coordinates.shape
		#print ("ranges_array clipped", ranges_array)



#===============detection ref========================
# In this part the open spaces are determined and the element of the ranges vector corresponding to the reference point is stored in an array.



		if ranges_array[0] == max_Range:
			ranges_array[0] = max_Range - 0.01
		elif ranges_array[719] == max_Range:
			ranges_array[719] = max_Range - 0.01

		count=0;
		nrpoint=0;

		for i in range(1,ranges_array.shape[0]):
			if ranges_array[i] == max_Range and ranges_array[i-1] == max_Range:
				pt1 = i
				count = count +1
			elif ranges_array[i] == max_Range:
				count = count +1
			elif ranges_array[i] != max_Range and ranges_array[i-1] == max_Range:
				if count >150:
					pt2 = i
					refdata[nrpoint]= ( (pt1+pt2)/2) 
					nrpoint = nrpoint + 1
					count = 0 
				else : 
					count = 0 

		#print refdata
		#refdata = numpy.asarray(refdata)
		#print refdata.shape
		print ("nrpoint",nrpoint)

		for n in range(nrpoint, 10):
			refdata[n] = 500
		print ("refdata",refdata)

def main():
    """ main function
    """
    node = sensing()

if __name__ == '__main__':
    while not rospy.is_shutdown() :
	    main()
 
