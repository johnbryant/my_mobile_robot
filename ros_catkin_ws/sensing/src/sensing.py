#!/usr/bin/env python

import rospy
import math
import numpy
from sensor_msgs.msg import LaserScan
import struct 
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker

GAP_SIZE = 100
MAX_RANGE = 3
MIN_RANGE = 0.75 
MAX_SMALLER_PERCENT = 0.4
MAX_RANGE_STEP = 0.1


class sensing:
	def __init__(self):
	        rospy.init_node('sense_robot_node', anonymous=True)   	   
        	#self.pub_move = rospy.Publisher('/marvin/diff_drive_controller/cmd_vel', Twist, queue_size=10)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
	    	self.pub_obj1 = rospy.Publisher("/marker_basic",Marker,queue_size=1)
	    	#self.rate = rospy.Rate(1)
	def callback (self,msg):
		global Ranges
		global angle_min
		global angle_increment
		global range_min
		Ranges = msg.ranges
		angle_min = msg.angle_min
		angle_increment = msg.angle_increment
		range_min = msg.range_min
		ranges_array =  numpy.asarray(Ranges)

		marker = Marker()
	        marker.header.frame_id = "laser_link"
	        marker.type = marker.POINTS
	    	marker.action = marker.ADD

	    	# marker scale
	    	marker.scale.x = 0.15
	    	marker.scale.y = 0.15
	    	marker.scale.z = 0.1
	    	#50,205,50
	    	# marker color
	    	marker.color.a = 1.0
	    	marker.color.r = 256.0
	    	marker.color.g = 0.0
	    	marker.color.b = 0.0

	    	# marker orientaiton
	    	marker.pose.orientation.x = 0.0
	    	marker.pose.orientation.y = 0.0
	    	marker.pose.orientation.z = 0.0
	    	marker.pose.orientation.w = 0.0

	    	# marker position
	    	marker.pose.position.x = 0.0
	    	marker.pose.position.y = 0.0
	    	marker.pose.position.z = 0.0


		midpoint = self.test_sensing(ranges_array)
		x = 1*midpoint[0]
		y = 1*midpoint[1]
		first_line_point = Point()
		first_line_point.x = x
	   	first_line_point.y = y
	    	first_line_point.z = 0
 		marker.points.append(first_line_point)
	    	self.pub_obj1.publish(marker)

	#Counts the amount of lasers that have a lower distance than "range"
	def countLessRange(self,ranges_array,range1):
		#print len(ranges) 
		count = 0
		for i in range(0,ranges_array.shape[0]):
			if Ranges[i] < range1:
					count = count +1
		return count

	#Keeps decreasing the maximum detection range, until the percentage condition is met. The varying range detection part.
	def determineMaxRange(self, ranges):
		#print len(ranges)
		if (MAX_RANGE_STEP <= 0):
        		print("Sensing in undefined state! Deadlock, stepsize <= 0 for maxRange finding!");
    		if (MAX_SMALLER_PERCENT > 1):
        		print("Sensing in undefined state! maxSmallerPercent > 1!");
   		if (MAX_SMALLER_PERCENT < 0):
        		print("Sensing in undefined state! maxSmallerPercent < 0!");
		maxRange = MAX_RANGE
		
		while (maxRange  > MIN_RANGE) :
			if (self.countLessRange(ranges, maxRange) <= MAX_SMALLER_PERCENT *ranges.shape[0]):
			 	maxRange = maxRange - MAX_RANGE_STEP
	 		else:
	 			break
		#print ("maxRange in function",maxRange)
		return maxRange

	#Sets the laser-distances that are larger than the maxi2mum range equal to the maximum range.
	#Including the very high distances (<minrange).

	def clipRanges(self, ranges_array, minRange, maxRange):
		for i in range(0,ranges_array.shape[0]):
			#print ("ranges_array[]i",ranges_array[i])
			if ranges_array[i] < minRange or ranges_array[i] >maxRange :
				#print "mphkes"
				ranges_array[i] = maxRange

		return ranges_array
	
	#Returns the x and y-coordinates of a specific ray
	def calculateXY_1_beam(self, ranges, angle_min, angle_increment, ray):
		coordinates_list = []
		theta = angle_min + angle_increment * ray
		x_coordinate = math.cos((theta) * ranges[ray]) 
		y_coordinate = math.sin((theta) * ranges[ray])

		return x_coordinate, y_coordinate

	def calculateXY_many_beams(self,ranges_array, angle_min, angle_increment):
		coordinates_list_all_beams = []
		for ray in range(0,ranges_array.shape[0]): 
			#print ray 
			x,y = self.calculateXY_1_beam(ranges_array,angle_min,angle_increment,ray)	
			coordinates_list_all_beams.append((x,y))	

		coordinates_array_all_beams = numpy.asarray(coordinates_list_all_beams)
		#print ("coordinates_array_all_beams.shape", coordinates_array_all_beams.shape)
		return coordinates_array_all_beams



	#void EdgeRay(int right, int left):
	#	print "edgeRay"
		
	# Returns the laser-numbers of the two edges of a gap (for each gap).

	#struct 


	def findEdgeRays(self, ranges_array, maxRange):
		#print ranges_array
		openCount = 0 
		right = 0 
		left = -1 
		#global EdgeRay
		EdgeRay = [right, left]
		#print ("EdgeRay", EdgeRay)
		EdgeRays = []
		for i in range(1,ranges_array.shape[0]):
			#print i
			if ranges_array[i] == maxRange and ranges_array[i-1] != maxRange: #if it's a potential gap 
       				#print "we found our right edge!"										#AND the previous ray was a wall
				EdgeRay[0] = i -1
			if (ranges_array[i]!= maxRange or  i == ranges_array.shape[0] - 1 ) and (ranges_array[i - 1] == maxRange):
				#if it's a wall            OR   the last ray                    AND  the previous ray was a potential gap
				EdgeRay[1] = i
				#print " We found our left edge!"
				if (EdgeRay[1] < 0 or EdgeRay[0] < 0):
					print "Sensing in undefined state! Found a left edge without a right edge at ray"
				else: 
					EdgeRays.append((EdgeRay[0],EdgeRay[1]))
					#print "found edge-ray pair"
					EdgeRay[0] = -1
					EdgeRay[1] = -1
		EdgeRays_array = numpy.asarray(EdgeRays)
		return EdgeRays_array

	def compute_edgeRays(self,ranges_array):
		maxRange = self.determineMaxRange(ranges_array)
		clippedRanges = self.clipRanges(ranges_array, range_min, maxRange)
		edgeRays = self.findEdgeRays(clippedRanges, maxRange)
		return edgeRays

	
	def findEdgePoints(self,ranges_array):
		edgeRays = self.compute_edgeRays(ranges_array)
		maxRange = self.determineMaxRange(ranges_array)
		clippedRanges = self.clipRanges(ranges_array, range_min, maxRange)
		edgePoints = []
		for i in range(0,edgeRays.shape[0]):
			#leftPoint = self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,min(edgeRays[0]))#edgeRays[ii].left)
			leftPoint = self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,edgeRays[i][1])#edgeRays[ii].left)
			rightPoint =  self.calculateXY_1_beam(clippedRanges,angle_min,angle_increment,edgeRays[i][0])#edgeRays[ii].right)			
			edgePoints.append(rightPoint)
			edgePoints.append(leftPoint)

		edgePoints_array = numpy.asarray(edgePoints)
		return edgePoints_array


	#Returns which laser points are in a gap or not, only if the gap is big enough for Pico (=corridor).

	def findGapRays(self, edgeRays, rangesSize, openSpacePoints):
		isGapRays = []
		for m in range(0,rangesSize):
			isGapRays.append(False)
		
		for i in range(0,edgeRays.shape[0]):
			if (edgeRays[i][1] - edgeRays[i][0]) > openSpacePoints:

				for j in range((edgeRays[i][0]+1),edgeRays[i][1]):

					isGapRays[j] = True

		if isGapRays[0] == 1 :
			#print "Sensing in undefined state! First ray is a gap!" 
			print("")
		if len(isGapRays) > 0:
			if isGapRays[len(isGapRays) - 1] == 1 : 
				#print "Sensing in undefined state! Last ray is a gap!"
				print("")

		else :
			print "Sensing error"
		return isGapRays


	def computeGapRays(self, ranges_array, openSpacePoints):
		edgeRays = self.compute_edgeRays(ranges_array)
		gapRays = self.findGapRays(edgeRays, ranges_array.shape[0], openSpacePoints)
		return gapRays


	def countGaps(self, isGapRays):
		flips1 = 0
		isGapRays = numpy.asarray(isGapRays)
		for i in range(1,len(isGapRays)):
			if (isGapRays[i-1] != isGapRays[i]):

				flips1 = flips1 + 1
		gaps = flips1/2
		#print ("posa gaps vlepw re malaka", gaps)
		return gaps

	def findMidRays(self,isGapRays,edgeRays):
		midGapRays = [] 
		for i in range(0,edgeRays.shape[0]):
			#print ("i",i)
			if ((isGapRays[edgeRays[i][1] - 1] == True) and (isGapRays[edgeRays[i][0] +1 ] == True)):
				midGapRays.append(math.floor(edgeRays[i][0] + (edgeRays[i][1] - edgeRays[i][0]))/2) 
				#print "mphkes"

		
		return midGapRays

	def findMidPointEdgePoints(self, ranges_array):
		edgeRays = self.compute_edgeRays(ranges_array)
		isGapRays = self.computeGapRays(ranges_array, GAP_SIZE)
		leftRightPoints = []
		for i in range(0, edgeRays.shape[0]):
			if ((isGapRays[edgeRays[i][1] - 1] == True) and (isGapRays[edgeRays[i][0] + 1] == True)):
				leftPoint = self.calculateXY_1_beam(ranges_array, angle_min, angle_increment,edgeRays[i][1])
				rightPoint = self.calculateXY_1_beam(ranges_array, angle_min, angle_increment,edgeRays[i][0])

				leftRightPoints.append(leftPoint)
				leftRightPoints.append(rightPoint)

		leftRightPoints = numpy.asarray(leftRightPoints)
		return leftRightPoints


	def findMidPoints(self, ranges_array):
		midPoints = self.findMidRayPoints(ranges_array)
		return midPoints




	def findMidRayPoints(self, ranges_array) :
		midRays = self.computeMidRays(ranges_array)
		midRays = numpy.asarray(midRays)
		midRays = int(midRays)
		maxRange = self.determineMaxRange(ranges_array)
		clippedRanges = self.clipRanges(ranges_array, range_min, maxRange)
		midRayPoints = self.calculateXY_1_beam(clippedRanges, angle_min, angle_increment, midRays)
		return midRayPoints
	

	def computeMidRays(self,ranges_array):
		edgeRays = self.compute_edgeRays(ranges_array)
		isGapRays = self.computeGapRays(ranges_array, GAP_SIZE)
		midRays = self.findMidRays(isGapRays, edgeRays)

		return midRays	






	def test_sensing(self, ranges_array):
		isGapRays = self.computeGapRays(ranges_array, GAP_SIZE)
   		#print (" isGapRays",isGapRays)
		numGaps = self.countGaps(isGapRays)
		print ("posa gaps exeis re malaka",numGaps)
    		midPoints = self.findMidPoints(ranges_array)
    		#print ("midPoints",midPoints)
    		return midPoints


def main():
    """ main function
    """
    node = sensing()

if __name__ == '__main__':
    while not rospy.is_shutdown() :
	    main()
 
