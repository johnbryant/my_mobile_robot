
"use strict";

let UInt16 = require('./UInt16.js');
let Float64MultiArray = require('./Float64MultiArray.js');
let Int32 = require('./Int32.js');
let Int8MultiArray = require('./Int8MultiArray.js');
let Bool = require('./Bool.js');
let Header = require('./Header.js');
let Int64MultiArray = require('./Int64MultiArray.js');
let UInt64 = require('./UInt64.js');
let UInt32MultiArray = require('./UInt32MultiArray.js');
let UInt8MultiArray = require('./UInt8MultiArray.js');
let MultiArrayLayout = require('./MultiArrayLayout.js');
let String = require('./String.js');
let Float32 = require('./Float32.js');
let MultiArrayDimension = require('./MultiArrayDimension.js');
let Char = require('./Char.js');
let UInt32 = require('./UInt32.js');
let Float64 = require('./Float64.js');
let Time = require('./Time.js');
let ColorRGBA = require('./ColorRGBA.js');
let Duration = require('./Duration.js');
let Int16 = require('./Int16.js');
let Float32MultiArray = require('./Float32MultiArray.js');
let UInt64MultiArray = require('./UInt64MultiArray.js');
let UInt8 = require('./UInt8.js');
let Byte = require('./Byte.js');
let Int32MultiArray = require('./Int32MultiArray.js');
let Int64 = require('./Int64.js');
let Empty = require('./Empty.js');
let ByteMultiArray = require('./ByteMultiArray.js');
let UInt16MultiArray = require('./UInt16MultiArray.js');
let Int16MultiArray = require('./Int16MultiArray.js');
let Int8 = require('./Int8.js');

module.exports = {
  UInt16: UInt16,
  Float64MultiArray: Float64MultiArray,
  Int32: Int32,
  Int8MultiArray: Int8MultiArray,
  Bool: Bool,
  Header: Header,
  Int64MultiArray: Int64MultiArray,
  UInt64: UInt64,
  UInt32MultiArray: UInt32MultiArray,
  UInt8MultiArray: UInt8MultiArray,
  MultiArrayLayout: MultiArrayLayout,
  String: String,
  Float32: Float32,
  MultiArrayDimension: MultiArrayDimension,
  Char: Char,
  UInt32: UInt32,
  Float64: Float64,
  Time: Time,
  ColorRGBA: ColorRGBA,
  Duration: Duration,
  Int16: Int16,
  Float32MultiArray: Float32MultiArray,
  UInt64MultiArray: UInt64MultiArray,
  UInt8: UInt8,
  Byte: Byte,
  Int32MultiArray: Int32MultiArray,
  Int64: Int64,
  Empty: Empty,
  ByteMultiArray: ByteMultiArray,
  UInt16MultiArray: UInt16MultiArray,
  Int16MultiArray: Int16MultiArray,
  Int8: Int8,
};
