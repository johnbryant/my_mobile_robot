#!/usr/bin/env python

#==========================================
# Title:  Quaternion to euler function
# Author: Ioannis Dionysios Bratis
# Date:   08.02.2019
#==========================================


import rospy
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler

roll = pitch = yaw = 0.0
PI = 3.1415926535897

def get_rotation(msg):
	global roll, pitch, yaw

	orientation_q = msg.pose.pose.orientation
	orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]

	(roll, pitch, yaw) = euler_from_quaternion(orientation_list) 
	yaw_degree = yaw *180/PI
	print yaw_degree



rospy.init_node('my_quaternion_to_euler')
sub = rospy.Subscriber('/marvin/diff_drive_controller/odom', Odometry, get_rotation)

while not rospy.is_shutdown():
	quat = quaternion_from_euler(roll, pitch, yaw)
