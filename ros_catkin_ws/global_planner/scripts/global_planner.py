#!/usr/bin/env python

import rospy
import math
import numpy






class global_planner:
	def __init__(self):
		area = raw_input("What is your area lane? ")
		target =  raw_input("What is your target? ")
		#print("area", area)
		#print ("target", target)
		route_lanes_list = self.route_planner(area, target)
		#print ("lanes_list",route_lanes_list)


	def route_planner(self,area,target):
	    	route_lanes_list = []
	    	if target == "racking1" or target == "racking2": 
	    		if (area == "lane1") or (area == "lane4"):
	    			route_lanes_list.append("junction1")
	    			route_lanes_list.append("aisle_lane1")

    			elif (area == "lane2") or (area == "lane3"):
    				route_lanes_list.append("junction2")
	    			route_lanes_list.append("aisle_lane2")
    			elif area == "junction1":
    				route_lanes_list.append("aisle_lane1")
				elif area == "junction2":
    				route_lanes_list.append("aisle_lane2")


	    	return route_lanes_list







def main():
    """ main function
    """
    node = global_planner()

if __name__ == '__main__':
    while not rospy.is_shutdown() :
	    main()
