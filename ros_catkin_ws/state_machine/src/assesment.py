#subscriber, publisher script 
#! /usr/bin/env python

import rospy                                          
from std_msgs.msg import String 

def callback(msg): 
      #sorted library already exists, does not have to be imported 
      message = sorted(msg)  
      pub.publish(message)

rospy.init_node('sub_node')

sub = rospy.Subscriber('/hello', String, callback)   
pub = rospy.Publisher('/sorted', String)
message = String()
                                                     
rospy.spin()  