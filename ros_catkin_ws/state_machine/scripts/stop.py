#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *

class stop:
	def __init__(self):

		#rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=10)
		#rospy.sleep(0.05)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
		#rospy.spin()
	def callback (self,msg):
		self.publishMessage()
	def publishMessage(self):
		msg = Twist()
		msg.angular.z = 0
		msg.linear.x = 0
		msg.linear.y = 0
		msg.linear.z = 0
		msg.angular.x = 0
		msg.angular.y = 0
		self.pub_vel.publish(msg)


 
