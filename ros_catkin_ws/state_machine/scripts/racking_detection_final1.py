#!/usr/bin/env python

import roslib
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist,Vector3
import random
import math
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion
from geometry_msgs.msg import Pose, PoseArray
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
import numpy
from visualization_msgs.msg import Marker
import matplotlib
import matplotlib.pyplot as plt
import numpy.matlib
import sys 



class Ransac(object):
	def __init__(self):
		# init the node
        	rospy.init_node('Ransac', anonymous=False)
	        self.pub_obj1 = rospy.Publisher("/RansacLineRight",Marker,queue_size=1)
	        self.pub_obj2 = rospy.Publisher("/RansacLineLeft",Marker,queue_size=1)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
		#slope = 0.01
		rospy.sleep(0)
	        rospy.spin()

	def calc_distance(self,random_point_cartesian_array,points1_transposed,new_number):
		kLine = numpy.array([[(random_point_cartesian_array[0][1] - random_point_cartesian_array[0][0])],[(random_point_cartesian_array[1][1] - random_point_cartesian_array[1][0])]]) # two points relative distance	
		
	
		kLineNorm = kLine/(numpy.linalg.norm(kLine)) 
		normVector = numpy.array([-kLineNorm[1],kLineNorm[0]])# Ax+By+C=0 A=-kLineNorm(2),B=kLineNorm(1)
		normVector = numpy.transpose(normVector)

		temp_sample= numpy.array([random_point_cartesian_array[:,0]])
		temp = numpy.transpose(temp_sample)
		distance = normVector.dot(points1_transposed - numpy.matlib.repmat(temp,1,new_number))
		return distance 

	def calc_param(self,random_point_cartesian_array):
		parameter1 = (random_point_cartesian_array[1][1]-random_point_cartesian_array[1][0])/(random_point_cartesian_array[0][1]-random_point_cartesian_array[0][0])

		parameter2 = random_point_cartesian_array[1][0] -parameter1*random_point_cartesian_array[0][0]

		bestParameter1=parameter1
		bestParameter2=parameter2
		return bestParameter1, bestParameter2


	def ransac_right_side(self,msg,pub_obj1): 
	    angle_min = msg.angle_min
	    angle_increment = msg.angle_increment
	    #iter: the number of iterations
	    #threshDist: the threshold of the distances between points and the fitting line
	    #inlierRatio: the threshold of the number of inliers 
	    
	    threshDist = 0.05
	    inlierRatio = 0.05
	    iter = 1000
	    
	    Ranges = msg.ranges
	    Ranges = numpy.asarray(Ranges)
	    for i in range(0,Ranges.shape[0]):
			if math.isnan(Ranges[i]):
				Ranges[i] = 500 
	    for i in range(0,Ranges.shape[0]/2):
			Ranges[i] = 5000
	    Ranges = numpy.ndarray.tolist(Ranges)
	    number = len(Ranges)# Total number of points


	    bestInNum = 0 #Best fitting line with largest number of inliers

	    bestParameter1=0
	    bestParameter2=0 # parameters for best fitting line

	    # in Marvin beam indices start from left side (0 left)
	    counter = 0
	    points_filtered = []
	    for r in Ranges:
		if (r < 2.5) and (r > 0.1): # distance smaller than 2 meters 
			x_coordinate = r * math.cos(angle_min + (angle_increment * counter) ) 
			y_coordinate = r * math.sin(angle_min + (angle_increment * counter) )
			z_coordinate = 0
			points_filtered.append((x_coordinate,y_coordinate))
		counter+=1
	    if len(points_filtered) == 0:
		print("no line from right side")
	    points_filtered= numpy.asarray(points_filtered)
	    number_of_filtered_points = points_filtered.shape[0]
	    points_filtered_transposed = numpy.transpose(points_filtered)
	    lists = []
	    bestlierIdx = []
	    for i in range(0,iter):
		    # Randomly select 2 points
			temp_points1 = numpy.transpose(points_filtered_transposed) 
			random_points_cartesian = random.sample(temp_points1,2)
			random_point_cartesian_array = numpy.array([[random_points_cartesian[0][0],random_points_cartesian[1][0]],[random_points_cartesian[0][1],random_points_cartesian[1][1]]])
		    # Compute the distances between all points with the fitting line 
			distance = self.calc_distance(random_point_cartesian_array,points_filtered_transposed,number_of_filtered_points)
	 
		        lists = []
	   		for k in range(0,distance.shape[1]-1):
				#append points whose distance is smaller than the threshold distance 
				if abs(distance[0,k]) <= threshDist:
					lists.append(k)	
			temp1=numpy.asarray(lists)
			inlierIdx = temp1
		    # Compute the inliers with distances smaller than the threshold
			inlierNum = inlierIdx.shape[0]
		    # Update the number of inliers and fitting model if better model is found     
		    	if (inlierNum >= round(inlierRatio*number_of_filtered_points)) and (inlierNum > bestInNum) :
		    	    bestInNum = inlierNum;
		    	    #compute parameters of line : y = a*x +b , a = bestParameter1, b = bestParameter2
		  	    bestParameter1,bestParameter2 =self.calc_param(random_point_cartesian_array)
		  	    bestlierIdx = inlierIdx

	    bestlierIdx = numpy.asarray(bestlierIdx)    
	    index2 = max(bestlierIdx)
	    index1 = min(bestlierIdx)
	    best_random_point1 = points_filtered_transposed[:,index1]	    
	    best_random_point2 = points_filtered_transposed[:,index2]
	    
	    #visualization in RVIZ 
	    marker1 = Marker()
	    marker1.header.frame_id = "laser_link"
	    marker1.type = marker1.LINE_STRIP
	    marker1.action = marker1.ADD

	    # marker scale
	    marker1.scale.x = 0.19
	    marker1.scale.y = 0
	    marker1.scale.z = 0
	    # marker color
	    marker1.color.a = 1.0
	    marker1.color.r = 0.0
	    marker1.color.g = 265.0
	    marker1.color.b = 0.0

	    # marker orientaiton
	    marker1.pose.orientation.x = 0.0
	    marker1.pose.orientation.y = 0.0
	    marker1.pose.orientation.z = 0.0
	    marker1.pose.orientation.w = 0.0

	    # marker position
	    marker1.pose.position.x = 0.0
	    marker1.pose.position.y = 0.0
	    marker1.pose.position.z = 0.0
	    marker1.points = []
	    # first point
	    first_line_point1 = Point()
	    first_line_point1.x = best_random_point1[0]
	    first_line_point1.y = best_random_point1[1]
	    first_line_point1.z = 0.0
	    marker1.points.append(first_line_point1)
	    # second point
	    second_line_point1 = Point()
	    second_line_point1.x = best_random_point2[0]
	    second_line_point1.y = best_random_point2[1]
	    second_line_point1.z = 0.0
	    marker1.points.append(second_line_point1)

	    # Publish the Marker
	    self.pub_obj1.publish(marker1)

	    length_of_line = math.sqrt( pow(first_line_point1.x - second_line_point1.x ,2) + pow(first_line_point1.y - second_line_point1.y,2) )

	    #print ("length of line ", length_of_line)
	    global slope

	    if (length_of_line <= (1.80 + 0.25)) and (length_of_line >= (1.80 - 0.25)):
	    	print ("I see a racking from the right side")
	    	slope = bestParameter1
	    	#print ("slope",slope)
	    else: 
		#print("bestParameter1",bestParameter1)
		#print("slope in smaller part of racking",slope)
		if (bestParameter1 >= slope - 0.1) and (bestParameter1 <= slope + 0.1):
		    #print("bestParameter1",bestParameter1)
		    print ("I still see a racking from the right side")
		    
		    
	def ransac_left_side(self,msg,pub_obj2): 
	    angle_min = msg.angle_min
	    angle_increment = msg.angle_increment
	    #iter: the number of iterations
	    #threshDist: the threshold of the distances between points and the fitting line
	    #inlierRatio: the threshold of the number of inliers 
	    
	    threshDist = 0.05
	    inlierRatio = 0.05
	    iter = 1000
	    
	    Ranges = msg.ranges
	    Ranges = numpy.asarray(Ranges)
	    for i in range(0,Ranges.shape[0]):
			if math.isnan(Ranges[i]):
				Ranges[i] = 500 
	    for i in range(Ranges.shape[0]/2,Ranges.shape[0]):
			Ranges[i] = 5000
	    Ranges = numpy.ndarray.tolist(Ranges)
	    number = len(Ranges)# Total number of points

	    #filter out right beams
	    
	    
	    bestInNum = 0 #Best fitting line with largest number of inliers

	    bestParameter1=0
	    bestParameter2=0 # parameters for best fitting line

	    # in Marvin beam indices start from left side (0 left)
	    counter = 0
	    points_filtered = []
	    for r in Ranges:
		if (r < 2.5) and (r > 0.1): # distance smaller than 2 meters 
			x_coordinate = r * math.cos(angle_min + (angle_increment * counter) ) 
			y_coordinate = r * math.sin(angle_min + (angle_increment * counter) )
			z_coordinate = 0
			points_filtered.append((x_coordinate,y_coordinate))
		counter+=1
	    if len(points_filtered) == 0:
		print("no line from left side")
	    points_filtered= numpy.asarray(points_filtered)
	    number_of_filtered_points = points_filtered.shape[0]
	    points_filtered_transposed = numpy.transpose(points_filtered)
	    lists = []
	    bestlierIdx = []
	    for i in range(0,iter):
		    # Randomly select 2 points
			temp_points1 = numpy.transpose(points_filtered_transposed) 
			random_points_cartesian = random.sample(temp_points1,2)
			random_point_cartesian_array = numpy.array([[random_points_cartesian[0][0],random_points_cartesian[1][0]],[random_points_cartesian[0][1],random_points_cartesian[1][1]]])
		    # Compute the distances between all points with the fitting line 
			distance = self.calc_distance(random_point_cartesian_array,points_filtered_transposed,number_of_filtered_points)
	 
		        lists = []
	   		for k in range(0,distance.shape[1]-1):
				#append points whose distance is smaller than the threshold distance 
				if abs(distance[0,k]) <= threshDist:
					lists.append(k)	
			temp1=numpy.asarray(lists)
			inlierIdx = temp1
		    # Compute the inliers with distances smaller than the threshold
			inlierNum = inlierIdx.shape[0]
		    # Update the number of inliers and fitting model if better model is found     
		    	if (inlierNum >= round(inlierRatio*number_of_filtered_points)) and (inlierNum > bestInNum) :
		    	    bestInNum = inlierNum;
		    	    #compute parameters of line : y = a*x +b , a = bestParameter1, b = bestParameter2
		  	    bestParameter1,bestParameter2 =self.calc_param(random_point_cartesian_array)
		  	    bestlierIdx = inlierIdx

	    bestlierIdx = numpy.asarray(bestlierIdx)    
	    index2 = max(bestlierIdx)
	    index1 = min(bestlierIdx)
	    best_random_point1 = points_filtered_transposed[:,index1]	    
	    best_random_point2 = points_filtered_transposed[:,index2]
	    
	    #visualization in RVIZ 
	    marker = Marker()
	    marker.header.frame_id = "laser_link"
	    marker.type = marker.LINE_STRIP
	    marker.action = marker.ADD

	    # marker scale
	    marker.scale.x = 0.19
	    marker.scale.y = 0
	    marker.scale.z = 0
	    # marker color
	    marker.color.a = 1.0
	    marker.color.r = 50.0
	    marker.color.g = 265.0
	    marker.color.b = 0.0

	    # marker orientaiton
	    marker.pose.orientation.x = 0.0
	    marker.pose.orientation.y = 0.0
	    marker.pose.orientation.z = 0.0
	    marker.pose.orientation.w = 0.0

	    # marker position
	    marker.pose.position.x = 0.0
	    marker.pose.position.y = 0.0
	    marker.pose.position.z = 0.0
	    marker.points = []
	    # first point
	    first_line_point = Point()
	    first_line_point.x = best_random_point1[0]
	    first_line_point.y = best_random_point1[1]
	    first_line_point.z = 0.0
	    marker.points.append(first_line_point)
	    # second point
	    second_line_point = Point()
	    second_line_point.x = best_random_point2[0]
	    second_line_point.y = best_random_point2[1]
	    second_line_point.z = 0.0
	    marker.points.append(second_line_point)

	    # Publish the Marker
	    self.pub_obj2.publish(marker)

	    length_of_line = math.sqrt( pow(first_line_point.x - second_line_point.x ,2) + pow(first_line_point.y - second_line_point.y,2) )

	    #print ("length of line ", length_of_line)
	    global slope

	    if (length_of_line <= (1.80 + 0.25)) and (length_of_line >= (1.80 - 0.25)):
	    	print ("I see a racking from the left side")
	    	slope = bestParameter1
	    	#print ("slope",slope)
	    else: 
		#print("bestParameter1",bestParameter1)
		#print("slope in smaller part of racking",slope)
		if (bestParameter1 >= slope - 0.1) and (bestParameter1 <= slope + 0.1):
		    #print("bestParameter1",bestParameter1)
		    print ("I still see a racking from the left side")
	def callback (self,msg):
		self.ransac_left_side(msg,self.pub_obj2)
		self.ransac_right_side(msg,self.pub_obj1)
		
	   
   

def main():
    """ main function
    """
    node = Ransac()

if __name__ == '__main__':
    main()
 
