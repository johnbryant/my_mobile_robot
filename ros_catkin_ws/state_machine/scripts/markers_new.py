#!/usr/bin/env python

import rospy
from visualization_msgs.msg import MarkerArray, Marker
import numpy
import random

import roslib
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, Polygon
from tf import transformations 
import rviz_tools_py as rviz_tools

class markers(object):
  def __init__(self):
	# init the node
	rospy.init_node('markers', anonymous=False)
	self.pub_obj1 = rospy.Publisher("/markers_wm",Marker,queue_size=1)
	rospy.spin()
	
  def marker_viz(self):
   marker = Marker()
   marker.header.frame_id = "/map"
   marker.type = marker.LINE_STRIP
   marker.action = marker.ADD
   marker.scale.x = 1.5
   marker.scale.y = 0.5
   marker.scale.z = 0
   # marker color
   marker.color.a = 0.0
   marker.color.r = 50.0
   marker.color.g = 0.0
   marker.color.b = 0.0
   # marker orientaiton
   marker.pose.orientation.x = 0.0
   marker.pose.orientation.y = 0.0
   marker.pose.orientation.z = 0.0
   marker.pose.orientation.w = 0.0
   # marker position
   marker.pose.position.x = 0.0
   marker.pose.position.y = 0.0
   marker.pose.position.z = 0.0
   
   first_line_point = Point()
   first_line_point.x = 1.0
   first_line_point.y = 1.0
   first_line_point.z = 0.0
   marker.points.append(first_line_point)
   # second point
   second_line_point = Point()
   second_line_point.x = 2.0
   second_line_point.y = 2.0
   second_line_point.z = 0.0
   marker.points.append(second_line_point)
   self.pub_obj1.publish(marker)
   print("malakaaa")

def main():
    """ main function
    """
    node = markers()
    node.marker_viz()

if __name__ == '__main__':
    main()