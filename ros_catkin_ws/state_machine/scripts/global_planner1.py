#!/usr/bin/env python
import rospy
import math
import numpy
#==========================================
# Title:  Global Planner sensing function
# Author: Ioannis Dionysios Bratis
# Date:   08.02.2019
#==========================================
class global_planner:
	def __init__(self):
		print ("")
	def lane_planner(self,area,target,junction):
	  #receives as input current robot area,junction area and target area and returns the areas to follow
	    	route_lanes_list = []
	    	if target == 'racking1' or target == 'racking2': 
	    		if (area == 'lane1') or (area == 'lane4'):
	    			route_lanes_list.append('junction1')
	    			route_lanes_list.append('aisle_area')
	    			route_lanes_list.append('aisle_lane1')
    			elif (area == 'lane2') or (area == 'lane3'):
    				route_lanes_list.append('junction2')
    				route_lanes_list.append('aisle_area')
	    			route_lanes_list.append('aisle_lane2')
    			elif area == 'junction1':
    				route_lanes_list.append('aisle_lane1')
			elif area == 'junction2':
    				route_lanes_list.append('aisle_lane2')
			elif (area == 'aisle_area' and junction == 'junction2'): 
				print("coming to aisle area from junction2")
				route_lanes_list.append('aisle_lane2')
			elif (area == 'aisle_area' and junction == 'junction1'):
				print("coming to aisle area from junction1")
				route_lanes_list.append('aisle_lane1')

	    	return route_lanes_list








