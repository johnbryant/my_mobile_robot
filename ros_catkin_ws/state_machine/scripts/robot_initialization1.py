#!/usr/bin/env python

#==========================================
# Title:  Robot initialization
# Author: Ioannis Dionysios Bratis
# Date:   08.02.2019
#==========================================


import rospy
from sensor_msgs.msg import Image
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
#initialize variables 
global Flag_odom ,Flag_rgb,Flag_depth,Flag_laser ,final
Flag_odom =0
Flag_rgb =0
Flag_depth=0
Flag_laser=0


class robot_init:
	def __init__(self):
	        #rospy.init_node('move_robot_node', anonymous=True)   	   
        	sub_odom = rospy.Subscriber ('/marvin/diff_drive_controller/odom', Odometry, self.callback_odom,queue_size=1)  
        	sub_rgb = rospy.Subscriber ('/camera/rgb/image_raw', Image, self.callback_rgb,queue_size=1)  
        	sub_depth = rospy.Subscriber ('/camera/depth/image_raw', Image, self.callback_depth,queue_size=1)  
        	sub_laserScanner = rospy.Subscriber ('/marvin/laser/scan', LaserScan, self.callback_laser,queue_size=1)  

			
	def callback_odom(self,msg):
		Flag_odom = 1
		return Flag_odom 
	def callback_rgb(self,msg):
		Flag_rgb = 1 
		return Flag_rgb 
	def callback_depth(self,msg):
		Flag_depth= 1 
		return Flag_depth
	def callback_laser(self,msg):
	        Flag_laser = 1 
		return Flag_laser
	def final_init (self ):
		msg= True
		if (self.callback_odom(msg) == 1 and self.callback_rgb (msg)==1 and self.callback_depth(msg)==1 and self.callback_laser(msg)==1):
			final = 1
			print "sensors initialized, lets get this party started"
			return True 
		else :
			return False 
			final =0
 
