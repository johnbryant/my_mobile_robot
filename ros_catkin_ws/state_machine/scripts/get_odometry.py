#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

#==========================================
# Title:  Get robot odometry
# Author: Ioannis Dionysios Bratis
# Date:   08.02.2019
#==========================================


class get_odometry:
	def __init__(self):

		#rospy.init_node('get_odometry', anonymous=False)
		self.sub = rospy.Subscriber ('/marvin/diff_drive_controller/odom', Odometry, self.get_odom)
	        #rospy.sleep(0)
		self.pose = Odometry()
		#rospy.spin()
	def get_odom (self,msg):
		
		self.pose = msg
		#print ("pose in get odometry",self.pose)
		
	

def main():
    """ main function
    """
    node = get_odometry()

if __name__ == '__main__':
    while not rospy.is_shutdown() :
	    main()