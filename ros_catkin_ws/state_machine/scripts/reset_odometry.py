#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry




class reset_odometry:
	def __init__(self):

		#rospy.init_node('reset_odometry', anonymous=False)
		self.sub = rospy.Subscriber ('/marvin/diff_drive_controller/odom', Odometry, self.reset_odom)
	        #rospy.sleep(0)

		self.pub = rospy.Publisher('odometry_data', Odometry, queue_size=1)
		#self.odom = Odometry()
		self.odom = ()
		self.pose = Odometry()
		#rospy.spin()
	#def get_odom (self,msg):
		#self.odom = msg
		#self.pose = msg
		#self.pub.publish(self.odom)
		#self.reset_odom()
		
	def reset_odom(self):
		self.odom = msg
		self.pose = msg
		#print ("x odom",self.pose.pose.pose.position.x)
		self.odom.pose.pose.position.x = self.odom.pose.pose.position.x - self.pose.pose.pose.position.x
		self.odom.pose.pose.position.y = self.odom.pose.pose.position.y - self.pose.pose.pose.position.y
		self.odom.pose.pose.position.z = self.odom.pose.pose.position.z - self.pose.pose.pose.position.z
		self.odom.pose.pose.orientation.x = self.odom.pose.pose.orientation.x - self.pose.pose.pose.orientation.x
		self.odom.pose.pose.orientation.y = self.odom.pose.pose.orientation.y - self.pose.pose.pose.orientation.y
		self.odom.pose.pose.orientation.z = self.odom.pose.pose.orientation.z - self.pose.pose.pose.orientation.z
		self.odom.pose.pose.orientation.w = self.odom.pose.pose.orientation.w - self.pose.pose.pose.orientation.w

		self.odom.twist.twist.linear.x = self.odom.twist.twist.linear.x - self.pose.twist.twist.linear.x
		self.odom.twist.twist.linear.y = self.odom.twist.twist.linear.y - self.pose.twist.twist.linear.y
		self.odom.twist.twist.linear.z = self.odom.twist.twist.linear.z - self.pose.twist.twist.linear.z
		self.odom.twist.twist.angular.x = self.odom.twist.twist.angular.x - self.pose.twist.twist.angular.x
		self.odom.twist.twist.angular.y = self.odom.twist.twist.angular.y - self.pose.twist.twist.angular.y
		self.odom.twist.twist.angular.z = self.odom.twist.twist.angular.z - self.pose.twist.twist.angular.z
		#print("reseting odometry",self.odom)
		self.pub.publish(self.odom)

#def main():
    #""" main function
    #"""
    #node = reset_odometry()

#if __name__ == '__main__':
    #while not rospy.is_shutdown() :
	    #main()