#!/usr/bin/env python

#==========================================
# Title:  State machine
# Author: Ioannis Dionysios Bratis
# Date:   08.02.2019
#==========================================


#import libraries and other scripts
import rospy
import smach
import smach_ros
from smach import CBState
import robot_initialization1 
import stay_in_the_lane_lazy_left1
import stay_in_the_lane_lazy_right1
import racking_detection_left1
import racking_detection_right1
import stop
import global_planner1 
import query1
import reset_odometry 
import get_odometry
import sensing1
import aruco_movement_combination1
import turn_left
import move_to_target
from std_msgs.msg import Empty
from std_msgs.msg import Empty

#initialize other scripts 
query_data = query1.query()
odometry = get_odometry.get_odometry()
sensing = sensing1.junction_detection()
aruco_combo = aruco_movement_combination1.combination()

#initialize junction position as empty
global junction_position
junction_position = ''
      
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['initialized','failed'])
def init_callback( userdata):
    #initialize your sensors
    init = robot_initialization1.robot_init ()
    if init.final_init() == True:
        print "Data is availble"
        return 'initialized'

    else:
        print "No data from sensors"
        return 'failed'

@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def docking_callback (userdata):
    #check if you are in the "docking area"
    docking_area = raw_input("Am I in the docking area? 1 for true/ 0 for false ")  
    if docking_area =="1" :
        global POSITION
    	POSITION ='lane2' 
        return 'yes'
    else :
            return 'no'

###################################################################### update positon
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def  update_position_callback (userdata): 
	# set initial position
	global POSITION 
	POSITION ='lane2' # lane 1 
	return 'yes'


@smach.cb_interface(input_keys=['flag_right','flag_left','target_in'], output_keys=[], outcomes=['yes'])
def  update_position_callback2 (userdata): 
	#check if you are still in the same area.
	global POSITION 
	global junction_position
	print ("position in update callback",POSITION)
	planner = global_planner1.global_planner()
	route = planner.lane_planner(POSITION,userdata.target_in,junction_position)
	if POSITION == 'lane2' and userdata.flag_left == 1:
	  print ("established that position is LANE2")
	  POSITION = 'lane2'
	elif POSITION == 'lane1' and userdata.flag_right == 1 :
	  print ("established that position is LANE1")
	  POSITION == ' lane1'
	else : 
	  print('error undefined position')
	if POSITION == 'aisle_area'and userdata.flag_left == 1 and userdata.flag_right == 1:
	  if (junction_position == 'junction2') and (route == 'aisle_lane1') :
	    print("correctly switched to aisle lane 2")
	    POSITION = 'aisle_lane2'
	  elif junction_position == 'junction1' and route == 'aisle_lane1':
	    print("correctly switched to aisle lane 1")
	    POSITION = 'aisle_lane1'
	return 'yes'

@smach.cb_interface(input_keys=['input_Route1','flag_left','flag_right','target_in'], output_keys=[''], outcomes=['yes'])
def  update_position_callback3 (userdata):
	#check if you have completed the current junction
	global POSITION
	global junction_position
	print("POSITION in update_callback 3",POSITION)


	if POSITION == 'junction2' and userdata.flag_left == 1 and userdata.flag_left == 1:
	  print("I am in junction 2 and possibly aisle area in front")
	  print ("position is aisle area from JUNCTION_2")
	  POSITION = 'aisle_area'
	elif POSITION == 'junction1' and userdata.flag_right == 1 and userdata.flag_left == 1 :
	  print ("position is aisle area from JUNCTION_1")
	  POSITION == 'aisle_area'
	else : 
	  print('error undefined position')
	return 'yes'
      
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def  update_position_callback4 (userdata): 
	#save POSITION after task completion before receiving new task 
	global POSITION 
	return 'yes'


@smach.cb_interface(input_keys=[], output_keys=['target_out'], outcomes=['yes','finished'])
def get_target_callback (userdata): 
	#receive target from user
	target =  raw_input("What is your target? racking 1 or 2")   
	userdata.target_out = target 
	if target == 'finished' :
	   return 'finished'
	else:
	   return 'yes'

@smach.cb_interface(input_keys=['target_in'], output_keys=['Route'], outcomes=['yes'])
def global_planner_callback (userdata):
	#receive areas to follow through global planner
	global POSITION
	planner = global_planner1.global_planner ()
    	print ("POSITION= ", POSITION , "target", userdata.target_in)
	route = planner.lane_planner(POSITION,userdata.target_in,junction_position)
	userdata.Route= route
	print ("Route= ", route)
        return 'yes' 

########################################################################### Moving and turning 
@smach.cb_interface(input_keys=['input_Route5','target_in'], output_keys=[], outcomes=['yes','no'])
def move_in_the_lane_callback (userdata):
    #basic skill of moving in lane
    global POSITION
    global junction_position
    global temp_odom_x
    global temp_odom_y
    #print('current position',POSITION)
    if POSITION == 'lane1'or POSITION == 'lane3' :#or POSITION =='aisle_lane1':
      print("move inside lane1")

      stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
      stay_in_the_lane_right
      return 'no'

    elif POSITION == 'lane2'or POSITION == 'lane4':# or POSITION == 'aisle_lane1':
      junction_counter = 0
      print("move inside lane2")
      stay_in_the_lane_left = stay_in_the_lane_lazy_left1.stay_in_the_lane()
      stay_in_the_lane_left
      odom_margin_x1 = 1.5
      odom_margin_y1 = 0.4
      print ("lets compare odometry")
      if ((odometry.pose.pose.pose.position.x > temp_odom_x + odom_margin_x1) or (odometry.pose.pose.pose.position.x < temp_odom_x - odom_margin_x1) and (odometry.pose.pose.pose.position.y > temp_odom_y + odom_margin_y1) or (odometry.pose.pose.pose.position.y < temp_odom_y - odom_margin_y1)):
	print ("possibly I am in junction")
	if sensing.gap_front == 1 and sensing.gap_left == 1 :
	  print ("detected junction, lets make a turn")
	  return 'yes'
	else :
	  return 'no'
      else:
	return 'no'
    elif POSITION == 'aisle_area':
      	planner1 = global_planner1.global_planner ()
	route = planner1.lane_planner(POSITION,userdata.target_in,junction_position)
	if route == 'aisle_lane1':
	  stay_in_the_lane_left = stay_in_the_lane_lazy_left1.stay_in_the_lane()
	  stay_in_the_lane_left
	  POSITION =  'aisle_lane1' 
	elif route == 'aisle_lane2':
	  stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
	  stay_in_the_lane_right
	  POSITION =  'aisle_lane2' 

	return 'no'
    elif POSITION == 'junction1' or POSITION == 'junction2':
	return 'yes'     
    else: 
      return 'no'
    
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def move_to_target_callback (userdata):
    #move to given target motion skill 
    positioning = move_to_target.stay_in_the_lane()
    positioning
    return 'yes'

@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def turn_callback (userdata):
    #turn inside junction motion skill 
    global POSITION
    global junction_position
    global temp_odom_x
    global temp_odom_y
    odom_margin_x = 1
    odom_margin_y = 1
    if POSITION == 'lane1':
      print("turn from lane1 to junction1")
      stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
      stay_in_the_lane_right
      POSITION = 'junction1'
      junction_position = 'junction1'
      if ((odometry.pose.pose.pose.position.x - temp_odom_x >  + odom_margin_x) or (odometry.pose.pose.pose.position.x - temp_odom_x < - odom_margin_x) and (odometry.pose.pose.pose.position.y - temp_odom_y > + odom_margin_y) or (odometry.pose.pose.pose.position.y - temp_odom_y < - odom_margin_y)):
	print("finished junction 1 ")
	return 'yes'
      else: 
	temp_odom_x = odometry.pose.pose.pose.position.x
	temp_odom_y = odometry.pose.pose.pose.position.y
	print("not finished junction 1 ")
	return 'no'
	  

    elif POSITION == 'lane2':
      print("turn from lane2 to junction2")
      #print("odometry in junction",odometry.pose.pose.pose.position.x)
      stay_in_the_lane_left = turn_left.stay_in_the_lane()
      stay_in_the_lane_left
      POSITION = 'junction2'
      junction_position = 'junction2'
      if ((odometry.pose.pose.pose.position.x - temp_odom_x >  + odom_margin_x) or (odometry.pose.pose.pose.position.x - temp_odom_x < - odom_margin_x) and (odometry.pose.pose.pose.position.y - temp_odom_y > + odom_margin_y) or (odometry.pose.pose.pose.position.y - temp_odom_y < - odom_margin_y)):
	print("finished junction 2 ")
	return 'yes'
      else: 
	print("not finished junction 2 ")
	return 'no'
      
    else :
      return 'no'

@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def stop_callback (userdata):
    #stop as soon as distance to target condition is met
    aruco1 = aruco_movement_combination1.combination()
    stop_robot = stop.stop()
    stop_robot
    if aruco1.distance_to_target < 1.2 :

      print ("I am stopping the robot")
      stop_robot = stop.stop()
      stop_robot
    return 'no'
      
############################################################################ Detecting

@smach.cb_interface(input_keys=[], output_keys=['detected_line_length_right','detected_line_length_left','detected_slope_right','detected_slope_left'], outcomes=['yes','no'])
def detect_feature_callback (userdata):
    #detect features (racking) for lanes and aisle area
    global POSITION
    line_left = 0
    line_right = 0
    if POSITION == 'lane1':
      line_right = racking_detection_right.line_length_right
      userdata.detected_line_length_right = racking_detection_right.line_length_right
      userdata.detected_slope_right = racking_detection_right.detected_slope_right
    
    elif POSITION == 'lane2':
      line_left = racking_detection_left.line_length_left
      userdata.detected_line_length_left = racking_detection_left.line_length_left
      userdata.detected_slope_left = racking_detection_left.detected_slope_left
    else :
      line_right = racking_detection_right.line_length_right
      line_left = racking_detection_left.line_length_left
      userdata.detected_line_length_right = racking_detection_right.line_length_right
      userdata.detected_line_length_left = racking_detection_left.line_length_left
      userdata.detected_slope_right = racking_detection_right.detected_slope_right
      userdata.detected_slope_left = racking_detection_left.detected_slope_left

    return 'yes'

@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def detect_target_callback (userdata):
    #detect target in aisle area
    stay_in_the_lane_left = turn_left.stay_in_the_lane()
    stay_in_the_lane_left
    aruco_marker_id = aruco_combo.aruco_marker_id
    aruco_distance_to_target = aruco_combo.distance_to_target
    aruco_angle_to_target = aruco_combo.angle_to_target
    print ("aruco aruco_marker_id", aruco_marker_id)
    if aruco_marker_id == 5000: 
      return 'no'
    else : 
        return 'yes'


############################################################################## Querying
@smach.cb_interface(input_keys=[], output_keys=['line_length_WM'], outcomes=['yes'])
def query_data_callback (userdata):
    #query relations for current position 
    relation = query_data.getWayRelations(POSITION)
    #userdata.line_length_WM = 1.8
    #query lenght and width of current position
    userdata.line_length_WM = query_data.getWayLength(relation[0][2])
    return 'yes'


@smach.cb_interface(input_keys=[''], output_keys=[], outcomes=['yes'])
def query_target_location_callback (userdata):
    global POSITION
    print ("POSITION in query target_location", POSITION)
    return 'yes'



@smach.cb_interface(input_keys=['target_in','detected_line_length_right','detected_slope_right','line_length_WM','detected_line_length_left','detected_slope_left','flag_racking_left','flag_racking_right'], output_keys=['flag_racking_left','flag_racking_right'], outcomes=['yes','no'])
def compare_data_callback (userdata):
    #compare detected line with line extracted from world model 
    slope_right = 0.00000
    slope_left = 0.00000
    flag_racking_right = 0
    flag_racking_left = 0
    userdata.flag_racking_right = 0
    userdata.flag_racking_left = 0 
    if (userdata.detected_line_length_right <= (userdata.line_length_WM + 0.25)) and (userdata.detected_line_length_right >= (userdata.line_length_WM - 0.25)):
	    	#print ("I see a racking from the right side")
	    	userdata.flag_racking_right = 1
	    	slope_right = userdata.detected_slope_right
    else: 
	  if (userdata.detected_slope_right >= slope_right - 0.2) and (userdata.detected_slope_right <= slope_right + 0.2):
	      #print ("I still see a racking from the right side")
	      userdata.flag_racking_right = 1
	      
    if (userdata.detected_line_length_left <= (userdata.line_length_WM + 0.25)) and (userdata.detected_line_length_left >= (userdata.line_length_WM - 0.25)):
	    	#print ("I see a racking from the left side")
	    	userdata.flag_racking_left = 1
	    	slope_left = userdata.detected_slope_right
    else: 
	  if (userdata.detected_slope_left >= slope_left - 0.2) and (userdata.detected_slope_left <= slope_left + 0.2):
	      #print ("I still see a racking from the left side")
	      userdata.flag_racking_left = 1
	      
    #temp = 1 
    #if temp == 1 :
    if flag_racking_right == 1 or flag_racking_left == 1:
	print("SUCCESS saw racking")
	return 'yes'
    else :
        return 'no'



@smach.cb_interface(input_keys=['input_target'], output_keys=[], outcomes=['yes','no'])
def check_target_lane_callback (userdata):
    #check if position of target is within aisle area
    global POSITION
    print ("POSITION in check target lane", POSITION)
    target = userdata.input_target
    print ("target in check_target ", target)
    if POSITION == 'aisle_area' : 
      return 'yes'
    else :
      return 'no'
      
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def positioning_callback (userdata):
    #properly position Marvin to target 
    aruco = aruco_movement_combination1.combination()
    if aruco.distance_to_target < 2.5 :
	print ("saw aruco")
	return 'yes'
    else :
	return 'no'
	positioning = move_to_target.stay_in_the_lane()
	positioning
    


if __name__ == '__main__':
    rospy.init_node('state_machine')
    # Create a SMACH state machine
    
    #inherite file classes and properties
    planner = global_planner1.global_planner ()
    racking_detection_left = racking_detection_left1.Ransac()
    racking_detection_right = racking_detection_right1.Ransac()
    
    sm = smach.StateMachine(outcomes=['outcome1','outcome2'])
    #initialize variables
    sm.userdata.Sm_Target = 'racking1'
    sm.userdata.Sm_neighbour=0
    sm.userdata.Sm_in1 = 0
    sm.userdata.Sm_in2 = 0
    sm.userdata.smRoute=0
    sm.userdata.sm_detected_line_right = 0
    sm.userdata.sm_detected_line_left = 0
    sm.userdata.sm_detected_slope_right = 0
    sm.userdata.sm_detected_slope_left = 0
    sm.userdata.sm_line_length_WM = 0
    sm.userdata.sm_flag_right_racking= 0
    sm.userdata.sm_flag_left_racking = 0
    global temp_odom_x, temp_odom_y
    temp_odom_x = odometry.pose.pose.pose.position.x
    temp_odom_y = odometry.pose.pose.pose.position.y
    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('state_machine', sm, '/SM_statemachine')
    sis.start()
    # Open the container
    with sm:
    # Add states to the container
        smach.StateMachine.add('INITIALIZATION', CBState(init_callback), {'initialized': 'DOCKING', 'failed':'INITIALIZATION'})
        smach.StateMachine.add('DOCKING', CBState(docking_callback), {'yes': 'UPDATE_POSITION', 'no':'outcome2'},remapping={'target_out':'Sm_Target'})
        smach.StateMachine.add('UPDATE_POSITION', CBState(update_position_callback), {'yes': 'GET_TARGET'})
        smach.StateMachine.add('GET_TARGET', CBState(get_target_callback), {'yes': 'GLOBAL_PLANNER','finished': 'outcome1'},remapping={})
        smach.StateMachine.add('GLOBAL_PLANNER', CBState(global_planner_callback), {'yes': 'QUERY_DATA'},remapping={'target_in':'Sm_Target','Route':'smRoute'})#,'Neighbour':'Sm_neighbour'})
        smach.StateMachine.add('QUERY_DATA', CBState(query_data_callback),{'yes': 'MOVE_IN_THE_LANE'},remapping={'input_Route5':'smRoute','line_length_WM':'sm_line_length_WM'})#,'input_neighbour': 'Sm_neighbour'})
        smach.StateMachine.add('MOVE_IN_THE_LANE', CBState(move_in_the_lane_callback),{'no':'DETECT_FEATURE','yes':'TURN'},remapping={'input_Route5':'smRoute','target_in':'Sm_Target'})
        smach.StateMachine.add('UPDATE_POSITION_3', CBState(update_position_callback3), {'yes': 'CHECK_TARGET_LANE'},remapping={'input_Route1':'smRoute','flag_left':'sm_flag_left_racking','flag_right':'sm_flag_right_racking'})
        smach.StateMachine.add('DETECT_FEATURE', CBState(detect_feature_callback),{'yes':'COMPARE_DATA', 'no':'MOVE_IN_THE_LANE'},remapping={'detected_line_length_right':'sm_detected_line_right','detected_line_length_left':'sm_detected_line_left','detected_slope_right':'sm_detected_line_right','detected_slope_left':'sm_detected_line_left'})
        smach.StateMachine.add('TURN', CBState(turn_callback), {'yes': 'UPDATE_POSITION_3','no':'MOVE_IN_THE_LANE'})
        smach.StateMachine.add('CHECK_TARGET_LANE', CBState(check_target_lane_callback),{'no': 'GLOBAL_PLANNER','yes':'QUERY_TARGET_LOCATION'},remapping={'input_Route2':'smRoute','input_target':'Sm_Target'})
        smach.StateMachine.add('QUERY_TARGET_LOCATION', CBState(query_target_location_callback),{'yes': 'DETECT_TARGET'},remapping={'input_Route4':'smRoute','input_target':'Sm_Target'})
	smach.StateMachine.add('MOVE_TO_TARGET', CBState(move_to_target_callback),{'yes': 'POSITIONING'})
	smach.StateMachine.add('DETECT_TARGET', CBState(detect_target_callback),{'no': 'DETECT_TARGET','yes':'MOVE_TO_TARGET'})
        smach.StateMachine.add('POSITIONING', CBState(positioning_callback),{'yes':'STOP','no':'POSITIONING'})
        smach.StateMachine.add('UPDATE_POSITION_4', CBState(update_position_callback4),{'yes':'GET_TARGET'})
        smach.StateMachine.add('COMPARE_DATA', CBState(compare_data_callback),{'no':'MOVE_IN_THE_LANE','yes': 'UPDATE_POSITION_2'},remapping={'target_in':'Sm_Target','detected_line_length_right':'sm_detected_line_right','detected_slope_right':'sm_detected_slope_right','line_length_WM':'sm_line_length_WM','detected_line_length_left':'sm_detected_line_left','detected_slope_left':'sm_detected_slope_left','flag_racking_left': 'sm_flag_left_racking','flag_racking_right':'sm_flag_right_racking'})
        smach.StateMachine.add('UPDATE_POSITION_2', CBState(update_position_callback2),{'yes': 'MOVE_IN_THE_LANE'},remapping={'flag_left':'sm_flag_left_racking','flag_right':'sm_flag_right_racking','target_in':'Sm_Target'})
        smach.StateMachine.add('STOP', CBState(stop_callback),{'yes': 'UPDATE_POSITION_4','no':'STOP'})

    sis = smach_ros.IntrospectionServer('state_machine', sm, '/SM_statemachine')
    sis.start()
    # Execute SMACH plan 
    outcome = sm.execute() 
    sis.stop()
