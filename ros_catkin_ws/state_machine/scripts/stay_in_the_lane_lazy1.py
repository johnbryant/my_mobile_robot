#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
#import move

direction = -1
wallDistance = 1
e=0
maxSpeed =1.2
P = 2
D = 0.3
angleCoef= 0.1
#mov = move.movement()
bound = 0.8
PI = 3.141592
class stay_in_the_lane:
	def __init__(self):

		#rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=10)
		rospy.sleep(2)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=1)
		#rospy.spin()
	def callback (self,msg):
		#self.ransac(msg.ranges,self.pub_obj1)	
		Ranges=msg.ranges
		angle_increment=msg.angle_increment
		size= len (msg.ranges)
		minIndex = size*(direction+1)/4;
  		maxIndex = size*(direction+3)/4;
		#print "minIndexb4"
		#print minIndex
		#print "range 0"
		#print Ranges[719]
		for i in range (minIndex,maxIndex):
			if Ranges[i]<Ranges[minIndex] and Ranges[i]>0:
				minIndex=i

		#print "minIndexafter"
		#print minIndex
		angleMin = (minIndex-size/2)*angle_increment	
		#print angleMin
		distMin=Ranges[minIndex]
		distFront= Ranges[size/2]

	





		
		self.publishMessage(distMin,distFront,angleMin)

	def publishMessage(self, distMin, distFront,angleMin):
		msg = Twist()
		if distMin < 0.7 or distFront < 0.7 :
			msg.angular.z = 1.0
			msg.linear.x = 0.1

			#print "you are leaving the lane from the right side, turn left"
			print "too close to the wall, turn left"

		elif distMin > 2 :

			msg.angular.z = -1.0
			msg.linear.x = 0.5
			print "you are leaving the lane from the left side, turn right"
		
		else:
			print "move forward you are in the lane "
			msg.linear.x = 0.9

		
		if msg.angular.z > bound:
			msg.angular.z = bound
		elif msg.angular.z < -bound : 
			msg.angular.z = -bound  

		#if (distFront < wallDistance):
		#	msg.linear.x = 0
		#	print "no linear velocity"
		#elif (distFront < wallDistance * 2):
		#	msg.linear.x = 0.4*maxSpeed
			#msg.linear.x = 0.4*mov.move_forward()

		#elif (abs(angleMin)>1.75):
		#	 msg.linear.x = 0.5*maxSpeed
 			 #msg.linear.x = 0.5*mov.move_forward()

		#else :
			 #msg.linear.x = mov.move_forward()
		#	 msg.linear.x = maxSpeed


		self.pub_vel.publish(msg)


 
