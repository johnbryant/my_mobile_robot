#! /usr/bin/env python


from geometry_msgs.msg import Twist
#from geometry_msgs.msg import Pose
#geometry_msgs/Pose

from aruco_pose.msg import Aruco
import numpy
import rospy
from tf.transformations import euler_from_quaternion, quaternion_from_euler
PI = 3.1415926535897

#aruco_pose/Aruco

class combination:

	def __init__(self):

		#rospy.init_node('combination', anonymous=False)
		self.aruco_marker_id = 5000
		self.distance_to_target = 5000
		self.angle_to_target = 0
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=1)
		rospy.sleep(1)
		self.sub = rospy.Subscriber('/ArUco_Marker', Aruco, self.callback,queue_size=1)
		#rospy.spin()
		
	def callback(self, msg):
		aruco_info = msg
		self.aruco_marker_id = aruco_info.marker_id
		#print("aruco_info",aruco_info)
		self.distance_to_target = aruco_info.pose.position.z
		#print("distance_to_target",self.distance_to_target)
		#w = aruco_info.pose.orientation.w
		#print("w",w)

		orientation_q = aruco_info.pose.orientation
		orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]

		(roll, pitch, yaw) = euler_from_quaternion(orientation_list) 
		self.angle_to_target = yaw *180/PI
		#print ("yaw_degree", self.angle_to_target)
		#self.move_to_target(self.distance_to_target,self.angle_to_target)

	def move_to_target(self,distance_to_target,angle_target):
		vel = Twist()
		if distance_to_target <= 0.4:
			vel.linear.x = 0
			vel.angular.z = 0
			self.pub_vel.publish(msg)



#def main():
    #node = combination()

#if __name__ == '__main__':
    #main()