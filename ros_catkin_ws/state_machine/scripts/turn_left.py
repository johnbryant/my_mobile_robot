#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
import numpy
import math
PI = 3.141592

# select direction = 1 to follow right wall/racking , direction = -1 to follow left wall/racking 
direction = -1
e=0
#select P,D, angleCoef values for for closed loop controller
P = 1
D = 0.3
angleCoef= 2
#select  maxSpeed for staying in the lane, closed loop controller
maxSpeed = 0.8
#select linear angular velocity for when Marvin is about to exit the lane
velocity_linear = 0.7
velocity_angular = 0.7
#set linear velocity for when Marvin is about to hit the wall
velocity_linear1  = -0.05
#set distance for marvin to be able to detect corner
corner_distance = 1.5
#set number of points above which, it is considered a corner 
number_of_corner_points = 500
#set velocity boundaries 
bound_linear = 0.3
bound_angular = 0.3
#initialize lane boundaries through Map
#min_lane_boundary = 1.0 #for lane2
#max_lane_boundary = 1.5 #for lane2

min_lane_boundary = 0.4 #for lane2
max_lane_boundary = 0.8 #for lane2
#select wallDistance for closed loop controller
wallDistance = 0.6
#set safety distance 
safety_distance = 0.3
class stay_in_the_lane:
	def __init__(self):
		#rospy.init_node('stay_in_the_lane', anonymous=Trues)   	   
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=1)
		rospy.sleep(1)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback)#,queue_size=1)
		#rospy.spin()
		self.e = 0
	def callback (self,msg):
		Ranges = msg.ranges
		ranges = numpy.asarray(Ranges)
		#filter out NaN values
		for i in range(0,ranges.shape[0]):
			if math.isnan(ranges[i]):
				ranges[i] = 5000 
		
		angle_increment = msg.angle_increment
		size = len(msg.ranges)
		minIndex = size*(direction+1)/4;
  		maxIndex = size*(direction+3)/4;
		#compare all values of laserscanner to find the min one
		temp = minIndex
		for i in range (minIndex,maxIndex):
			if (ranges[i] < ranges[temp]) and (ranges[i]>0) :
				temp = i

		minIndex = temp 
		angleMin = (minIndex-size/2)*angle_increment	
		distMin = ranges[minIndex]
		distFront= ranges[size/2]
		diffE = (distMin - wallDistance) - self.e
		self.e = distMin - wallDistance
		self.publishMessage(distMin,distFront,angleMin,ranges,minIndex,maxIndex,self.e,diffE)
		 
	      
	def publishMessage(self, distMin, distFront,angleMin, ranges, minIndex, maxIndex,e,diffE):
		msg = Twist()
		#safety first!! if anything at a distance smaller than safety_distance stop 
		for i in range(0,ranges.shape[0]/4):
			if ranges[i] < safety_distance and ranges[i] > 0.05 :
				print("which beam",i) 
				print("ranges[i]",ranges[i])
				msg.linear.x = 0
				msg.angular.z = 0
				rospy.loginfo ("you are about to crash dummie stop")
				print ("msg.linear.x",msg.linear.x)
				print ("msg.linear.z",msg.linear.z)
				self.pub_vel.publish(msg)
				return
		
		counter = 0
		for i in range(0,ranges.shape[0]):
			if ranges[i] > corner_distance:   
				counter = counter + 1

		if (distMin < min_lane_boundary or distFront < min_lane_boundary) and (distMin > 0.05 or distFront > 0.05):
		#check if you are about to exit the lane from the right boundary
			
			msg.angular.z = -velocity_angular
			msg.linear.x = velocity_linear1
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)

			self.pub_vel.publish(msg)
			#rospy.loginfo("too close to the wall, turn right")
			#print "too close to the wall, turn right"

		elif distMin > max_lane_boundary :
		#check if you are about to exit the lane from the left boundary
			msg.angular.z = +velocity_angular
			msg.linear.x = velocity_linear/3
			#rospy.loginfo("you are leaving the lane from the right side, turn left")
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			self.pub_vel.publish(msg)
			
		elif counter > number_of_corner_points:
		#check number of points that have distance > corner_distance meters. if bigger than >number_of_corner_points this go to closed-loop behavior
			#rospy.loginfo("corner coming up, brace yourselves!!!")
			msg.angular.z = direction*(P*e + D*diffE) + angleCoef * (angleMin - PI*direction/2)
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)

			if (distFront < wallDistance):
				msg.linear.x = 0
			elif (distFront < wallDistance * 2):
				msg.linear.x = 0.5*maxSpeed
			elif (abs(angleMin)>1.75):
				msg.linear.x = 0.5*maxSpeed
			else :
				msg.linear.x = maxSpeed
			
			
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			self.pub_vel.publish(msg)
		
		else:
		# you are within the boundaries of the lane 
			#rospy.loginfo("move forward you are in the lane")
			msg.linear.x = maxSpeed
			msg.angular.z = 0.0
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			self.pub_vel.publish(msg)
			
	def bound_velocity(self,linear_velocity,angular_velocity):
		#bound linear and angular of MARVIN 
		if angular_velocity > bound_angular:
			angular_velocity = bound_angular
		elif angular_velocity < -bound_angular : 
			angular_velocity = -bound_angular
		if linear_velocity > bound_linear:
			linear_velocity = bound_linear
		elif linear_velocity < -bound_linear : 
			linear_velocity = -bound_linear
		return linear_velocity,angular_velocity

#def main():
    #""" main function
    #"""
    #node = stay_in_the_lane()

#if __name__ == '__main__':
    #while not rospy.is_shutdown() :
	    #main()
