#!/usr/bin/env python

import rospy
from visualization_msgs.msg import MarkerArray, Marker
import numpy
import random

import roslib
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion, Vector3, Polygon
from tf import transformations 
import rviz_tools_py as rviz_tools

# Initialize the ROS Node
rospy.init_node('test', anonymous=False, log_level=rospy.INFO, disable_signals=False)


# Define exit handler
def cleanup_node():
    print "Shutting down node"
    markers.deleteAllMarkers()

rospy.on_shutdown(cleanup_node)

markers = rviz_tools.RvizMarkers('/map', 'visualization_marker')


while not rospy.is_shutdown():

    #Publish a line between two ROS Point Msgs
    #racking 1
    point1 = Point(58.3,27.2,0)
    point2 = Point(56.3,26.6,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime

    point1 = Point(58.3,27.2,0)
    point2 = Point(58.1,27.65,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime


    point1 = Point(58.1,27.65,0) 
    point2 = Point(56.1,27.0,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime


    point1 = Point(56.1,27.0,0) 
    point2 = Point(56.3,26.6,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime


    #racking 2
    point1 = Point(55.5,29.7,0)
    point2 = Point(57.3,30.3,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime

    point1 = Point(55.5,29.7,0)
    point2 = Point(55.4,30.0,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime


    point1 = Point(55.4,30.0,0) 
    point2 = Point(57.1,30.6,0)     
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime

    point1 = Point(57.1,30.6,0)     
    point2 = Point(57.3,30.3,0) 
    width = 0.05
    markers.publishLine(point1, point2, 'green', width, 5.0) # point1, point2, color, width, lifetime

    #aisle area
    point1 = Point(55.5,29.7,0)
    point2 = Point(57.3,30.3,0) 
    width = 0.08
    markers.publishLine(point1, point2, 'blue', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point1 = Point(58.1,27.65,0) 
    point2 = Point(56.1,27.0,0) 
    width = 0.08
    markers.publishLine(point1, point2, 'blue', width, 5.0) # point1, point2, color, width, lifetime


    point1 = Point(55.5,29.7,0)
    point2 = Point(56.3,27.1,0) 
    width = 0.08
    markers.publishLine(point1, point2, 'blue', width, 5.0) # point1, point2, color, width, lifetime

    point1 = Point(57.3,30.3,0) 
    point2 = Point(58.1,27.65,0) 
    width = 0.08
    markers.publishLine(point1, point2, 'blue', width, 5.0) # point1, point2, color, width, lifetime

    point1 = Point(55.9,28.4,0) 
    point2 = Point(57.7,28.9,0)     
    width = 0.05
    markers.publishLine(point1, point2, 'grey', width, 5.0) # point1, point2, color, width, lifetime

    #junction 2
    point1 = Point(57.3,30.3,0) 
    point2 = Point(58.1,27.65,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(57.3,30.3,0) 
    point2 = Point(58.6,30.7,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
     
    point1 = Point(58.6,30.7,0) 
    point2 = Point(59.6,28.0,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point1 = Point(58.1,27.65,0) 
    point2 = Point(59.6,28.0,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
     #junction 1
    point1 = Point(55.5,29.7,0)
    point2 = Point(56.3,27.1,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(55.5,29.7,0)
    point2 = Point(54.1,29.2,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
     
    point1 = Point(56.3,27.1,0) 
    point2 = Point(54.9,26.6,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(54.1,29.2,0) 
    point2 = Point(54.9,26.6,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'yellow', width, 5.0) # point1, point2, color, width, lifetime
    
    
    #lane 2
    point1 = Point(54.9,26.6,0) 
    point2 = Point(55.6,24.4,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point2 = Point(60.4,25.7,0) 
    point1 = Point(55.6,24.4,0) 
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    rospy.Rate(1).sleep() #1 Hz


    point1 = Point(60.4,25.7,0) 
    point2 = Point(59.6,28.0,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    rospy.Rate(1).sleep() #1 Hz

    point1 = Point(58.9,27.8,0) 
    point2 = Point(59.6,28.0,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(58.9,27.8,0) 
    point2 = Point(59.3,26.7,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(56.0,25.7,0) 
    point2 = Point(59.3,26.7,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point1 = Point(56.0,25.7,0) 
    point2 = Point(55.6,26.8,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    #lane 3,4
    point1 = Point(58.6,30.7,0) 
    point2 = Point(57.8,32.7,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(53.4,31.3,0) 
    point2 = Point(57.8,32.7,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(53.4,31.3,0) 
    point2 = Point(54.1,29.2,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point1 = Point(54.8,29.4,0) 
    point2 = Point(54.1,29.2,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(54.8,29.4,0) 
    point2 = Point(54.35,30.8,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    point1 = Point(57.5,31.8,0) 
    point2 = Point(54.35,30.8,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    
    point1 = Point(57.5,31.8,0) 
    point2 = Point(58,30.5,0)     
    width = 0.07
    markers.publishLine(point1, point2, 'pink', width, 5.0) # point1, point2, color, width, lifetime
    
    rospy.Rate(1).sleep() #1 Hz
   