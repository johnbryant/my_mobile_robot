#! /usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf import transformations
from std_srvs.srv import *
import numpy
import math

direction = -1
wallDistance = 1.0
e=0

P = 1
D = 0.3
angleCoef= 2
bound_linear = 0.8
bound_angular = 0.2
PI = 3.141592


maxSpeed = 0.8
velocity_linear = 0.7
velocity_angular = 0.7
velocity_linear_in_lane = 0.7
class stay_in_the_lane:
	def __init__(self):

		#rospy.init_node('stay_in_the_lane', anonymous=False)
	        self.pub_vel = rospy.Publisher("/marvin/diff_drive_controller/cmd_vel",Twist,queue_size=5)
		rospy.sleep(0)
		self.sub = rospy.Subscriber('/marvin/laser/scan', LaserScan, self.callback,queue_size=3)
		#rospy.spin()
	def callback (self,msg):
		Ranges = msg.ranges
		#print ("Ranges[0]",Ranges[0])
		ranges = numpy.asarray(Ranges)
		for i in range(0,ranges.shape[0]):
			if math.isnan(ranges[i]):
				ranges[i] = 5000 
		
		angle_increment = msg.angle_increment
		size = len(msg.ranges)
		minIndex = size*(direction+1)/4;
		#print ("minIndex before",minIndex)
  		maxIndex = size*(direction+3)/4;
		#print ("maxIndex before",maxIndex)

		#print ("ranges(406)",ranges[406])
		temp = minIndex
		for i in range (minIndex,maxIndex):
			if (ranges[i] < ranges[temp]) and (ranges[i]>0) :
				temp = i

		minIndex = temp 
		#print ("minIndex after",minIndex)

		angleMin = (minIndex-size/2)*angle_increment	
		#print ("angleMin", angleMin*180/PI)
		#error = angleMin - PI*direction/2
		#print ("error in degrees ",error*180/PI)


		#print ("angleMin", angleMin)

		distMin = ranges[minIndex]
		#print("minIndex",minIndex)
		distFront= ranges[size/2]
		global e
		diffE = (distMin - wallDistance) - e
		e = distMin - wallDistance
		self.publishMessage(distMin,distFront,angleMin,ranges,minIndex,maxIndex,e,diffE)
		 
	def bound_velocity(self,linear_velocity,angular_velocity):
		#bound linear and angular of MARVIN 
		bound_linear = 0.8
		bound_angular = 0.8
		if angular_velocity > bound_angular:
			angular_velocity = bound_angular
		elif angular_velocity < -bound_angular : 
			angular_velocity = -bound_angular
		if linear_velocity > bound_linear:
			linear_velocity = bound_linear
		elif linear_velocity < -bound_linear : 
			linear_velocity = -bound_linear
		#print ("linear_velocity",linear_velocity)
		#print ("angular_velocity",angular_velocity)
		return linear_velocity,angular_velocity
	      
	      
	#def safe_control(self, ranges,msg):
		##msg = Twist()
		#for i in range(0,ranges.shape[0]/2):
			#if ranges[i] < 0.2 : 
				#msg.linear.x = 0
				#msg.linear.z = 0
				#print ("you are about to crash dummie stop")
		#self.pub_vel.publish(msg)
	  
	#def myhook(self):
	    #print "shutdown time!"
	def publishMessage(self, distMin, distFront,angleMin, ranges, minIndex, maxIndex,e,diffE):
		msg = Twist()
		for i in range(0,ranges.shape[0]/4):
			if ranges[i] < 0.4 and ranges[i] > 0.05 :
				print("which beam",i) 
				print("ranges[i]",ranges[i])
				msg.linear.x = 0
				msg.angular.z = 0
				print ("you are about to crash dummie stop")
				print ("msg.linear.x",msg.linear.x)
				print ("msg.linear.z",msg.linear.z)
				self.pub_vel.publish(msg)
				return
		
		counter = 0
		for i in range(0,ranges.shape[0]):
			if ranges[i] > 1.5:#0.9 + 0.1:   
				counter = counter + 1

		print ("counter",counter)
		#print ("distMin", distMin)
		
			

		if distMin < 0.8 or distFront < 0.8:
			velocity_linear1  = -0.05
			#msg.angular.z = velocity_angular/3
			#msg.linear.x = velocity_linear1
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			#msg.angular.z = velocity_angular/2
			#msg.linear.x = velocity_linear1
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)s
			#msg.angular.z = 2*velocity_angular/3
			#msg.linear.x = 2*velocity_linear1
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			msg.angular.z = -velocity_angular
			msg.linear.x = velocity_linear1
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)

			self.pub_vel.publish(msg)
			print "too close to the wall, turn left"

		elif distMin > 1.3 :
			#msg.angular.z = -velocity_angular/3
			#msg.linear.x = velocity_linear/3
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)	
			#self.pub_vel.publish(msg)
			#msg.angular.z = -velocity_angular/2
			#msg.linear.x = velocity_linear/3
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			#msg.angular.z = -2*velocity_angular/3
			#msg.linear.x =velocity_linear/3
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			msg.angular.z = +velocity_angular
			msg.linear.x = velocity_linear/3
			print "you are leaving the lane from the left side, turn right"
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#print("msg.linear.x",msg.linear.x)
			#print (" msg.angular.z", msg.angular.z)
			self.pub_vel.publish(msg)
			
		elif counter > 500:#650 :
			print ("corner coming up, brace yourselves!!!")
			msg.angular.z = -direction*(P*e + D*diffE) + angleCoef * (angleMin - PI*direction/2)
			if msg.angular.z > bound_angular:
				msg.angular.z = bound_angular
			elif msg.angular.z < -bound_angular : 
				msg.angular.z = -bound_angular  

			if (distFront < wallDistance):
				msg.linear.x = 0
			elif (distFront < wallDistance * 2):
				msg.linear.x = 0.5*maxSpeed
				#msg.linear.x = 0.4*mov.move_forward()

			elif (abs(angleMin)>1.75):
				msg.linear.x = 0.5*maxSpeed
				#msg.linear.x = 0.5*mov.move_forward()

			else :
				msg.linear.x = maxSpeed
			
			
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			self.pub_vel.publish(msg)
		
		else:
			print "move forward you are in the lane "
			#print("[minIndex]",minIndex)
			#msg.linear.x = maxSpeed/3
			#msg.angular.z = 2*velocity_angular/3
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			#msg.linear.x = maxSpeed/2
			#msg.angular.z = velocity_angular/2
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			#msg.linear.x = 2*maxSpeed/3
			#msg.angular.z = velocity_angular/3
			#msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			#self.pub_vel.publish(msg)
			msg.linear.x = maxSpeed
			msg.angular.z = 0.0
			msg.linear.x,msg.angular.z = self.bound_velocity(msg.linear.x,msg.angular.z)
			self.pub_vel.publish(msg)


 
