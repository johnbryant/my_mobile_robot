import overpass
import rospy
import utm
import numpy
import math

"This file is maintained by Bratis, Haj Mustafa and Winters"

class query:
    """This class contains several definitions to perform overpass web api queries.
    In your script import this file:
    from query import query
    Then create an instance of the class 'query':
    test_class_instance = query()
    
    EXAMPLE:
    from query import query
    test = query()
    print("Coordinates of node 36:",test.getCoordinates(36))"""

    def __init__(self):
	# Gets the overpass api web server
        self.api = overpass.API(endpoint= 'http://localhost/api/interpreter',
				debug=False)
	# Create empty origin array
	self.origin = numpy.zeros(shape=(2))
	
    def setOrigin(self, node_ID):
	# sets node_ID as the origin of the local coordinate system
	print("The origin of your local coordinate system is node {}".format(node_ID))
	latlon_coordinates = self.getCoordinates(node_ID)
	origin = self.getUtmCoordinates(latlon_coordinates)
	self.origin = origin.astype(numpy.float)

    def getCoordinates(self, node_ID):
	# returns the coordinates of a specific node_ID
	query = "node(%d)" % node_ID
	response = self.api.get(query,"json")
	node_info = response["elements"][0]
	latitude = node_info["lat"]
	longitude = node_info["lon"]
	coordinates = []
	coordinates.append(latitude)
	coordinates.append(longitude)
	coordinates = numpy.asarray(coordinates)
	return coordinates
    
    def getLocalCoordinates(self, node_ID):
	# returns the local coordinates of node_ID
	if self.origin[0] == 0. and self.origin[1] == 0.:
	    print("ERROR: You have not set an origin of the local coordinate system!")
	    return
	latlon_coordinates = self.getCoordinates(node_ID)
	utm_coordinates = self.getUtmCoordinates(latlon_coordinates)
	utm_coordinates = utm_coordinates.astype(numpy.float)
	local_coordinates = numpy.subtract(utm_coordinates,self.origin)
	return local_coordinates
	
    
    def getUtmCoordinates(self, latlon_coordinates):
	# returns the UTM coordinates of a specific node based on lattitude and longitude
	coordinates = numpy.asarray(utm.from_latlon(latlon_coordinates[0], latlon_coordinates[1]))
	coordinates = coordinates[0:2]
	return coordinates
    
    def getWayID(self, way_name):
	# returns the ID of a way with key-value type: name = way_name
	relation = '[name="{}"]'.format(way_name)
	query = "(way{})".format(relation)
	response = self.api.get(query,"json")
	ID = response["elements"][0]["id"]
	return ID
	
    def getWayName(self, way_ID):
	# returns the name of the way with ID 'way_ID'
	query = 'way({})'.format(way_ID)
	response = self.api.get(query,"json")
	way_name = response["elements"][0]["tags"]["name"]
	return way_name
    
    def getNodeID(self, node_name):
	# returns the ID of a node with key-value type: name = node_name
	relation = '[name="{}"]'.format(node_name)
	query = "(node{})".format(relation)
	response = self.api.get(query,"json")
	ID = response["elements"][0]["id"]
	return ID
    
    def getNodeName(self, node_ID):
	# returns the name of the node with ID 'node_ID'
	query = 'node({})'.format(node_ID)
	response = self.api.get(query,"json")
	node_name = response["elements"][0]["tags"]["name"]
	return node_name
	
    def getWayNodes(self, way_name):
	# returns the nodes of a specic way with key-value type: name = way_name
	relation = '[name="{}"]'.format(way_name)
	query = "(way{});(._;>;);".format(relation)
	response = self.api.get(query,"json")
	way_node_IDs = []
	for element in response["elements"]:
	    if element["type"] == "node":
		way_node_IDs.append(element["id"])
	return way_node_IDs
    
    def getWayRelations(self,way_name):
	# returns the relations connected to the way with name = way_name
	relations = []
	relation = '[name="{}"]'.format(way_name)
	query = "(way{});(._;<;);".format(relation)
	response = self.api.get(query,"json")
	way_ID = self.getWayID(way_name)
	for element in response["elements"]:
	    if element["type"] == "relation":
		# make difference between one-way and two-way relations
		if element["tags"]["type"] == "has_a":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "has_landmark":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "contains":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "connected_to":
		    relations.append(self.evaluateTwoWayRelation(element,way_ID))
		else:
		    print("This relation type is currently not supported:",element["tags"]["type"])
	return relations
	
    def getNodeRelations(self,node_name):
	# returns the relations connected to the node with name = node_name
	relations = []
	relation = '[name="{}"]'.format(node_name)
	query = "(node{});(._;<;);".format(relation)
	response = self.api.get(query,"json")
	node_ID = self.getNodeID(node_name)
	for element in response["elements"]:
	    if element["type"] == "relation":
		# make difference between one-way and two-way relations
		if element["tags"]["type"] == "has_a":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "has_landmark":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "contains":
		    relations.append(self.evaluateOneWayRelation(element))
		elif element["tags"]["type"] == "connected_to":
		    relations.append(self.evaluateTwoWayRelation(element,node_ID))
		else:
		    print("This relation type is currently not supported:",element["tags"]["type"])
	return relations
    
    def evaluateOneWayRelation(self,element):
	# returns a one-way relation as a list with 'parent','relation','child'
	relation = [None] * 3
	relation[1] = element["tags"]["type"]
	for member in element["members"]:
	    if member["role"] == "parent":
		if member["type"] == "way":
		    relation[0] = self.getWayName(member["ref"])
		elif member["type"] == "node":
		    relation[0] = self.getNodeName(member["ref"])
	    elif member["role"] == "child":
		if member["type"] == "way":
		    relation[2] = self.getWayName(member["ref"])
		elif member["type"] == "node":
		    relation[2] = self.getNodeName(member["ref"])
	    else:
		print("ERROR: Relation has no role specified", element)
		return
	return relation
	
    def evaluateTwoWayRelation(self,element,ID):
	# returns a two-way relation as a list with 'queried element','relation','other element'
	relation = [None] * 3
	relation[1] = element["tags"]["type"]
	for member in element["members"]:
	    if member["ref"] != ID:
		if member["type"] == "way":
		    relation[2] = self.getWayName(member["ref"])
		elif member["type"] == "node":
		    relation[2] = self.getNodeName(member["ref"])
	    else:
		if member["type"] == "way":
		    relation[0] = self.getWayName(member["ref"])
		elif member["type"] == "node":
		    relation[0] = self.getNodeName(member["ref"])
	return relation
      
      
    def getLandmarksInWay(self,wayname):
        #returns a list of landmarks contained by a way
	landmarks_list=[]
	relations= self.getWayRelations(wayname)
	for i in range (0,len(relations)):
	  if relations [i][1] == 'has_landmark' : 
	    landmarks_list.append(relations [i][2])
	if len(landmarks_list) != 0:  
	  return landmarks_list
	else: 
	  print "This way doesn't have landmarks "
	  return landmarks_list
	
    def getTargetLocation(self,target,wayTarget,neighbourWay,origin):
	self.setOrigin(origin)
	shared_coordinates=[] 
	ID_landmark = self.getNodeID(target)
	landmark_corrdinate_local =self.getLocalCoordinates(ID_landmark)
	nodes = self.getWayNodes(wayTarget)
	nodes1 = self.getWayNodes(neighbourWay)
	for node in nodes:
	    x1=self.getLocalCoordinates(node)[0]
	    y1=self.getLocalCoordinates(node)[1]
	    for nodee in nodes1:
		x2=self.getLocalCoordinates(nodee)[0]
		y2=self.getLocalCoordinates(nodee)[1]
		if x1 == x2 and y1==y2 :
		  shared_coordinates.append(x1)
		  shared_coordinates.append(y1)
	sub_x1 = landmark_corrdinate_local[0]-shared_coordinates[0]
	sub_x2 = landmark_corrdinate_local[0]-shared_coordinates[2]
	sub_y1 = landmark_corrdinate_local[1]-shared_coordinates[1]
	sub_y2 = landmark_corrdinate_local[1]-shared_coordinates[3]
	shared_coordinates_diffX = abs (shared_coordinates[0]-shared_coordinates[2])
	shared_coordinates_diffY =abs (shared_coordinates[1]-shared_coordinates[3])
	if (shared_coordinates_diffX <= 1+0.2 and shared_coordinates_diffX >= 1-0.2):
	  Targetlocation=[0,abs(sub_y1)]
	  print ("Targetlocation",Targetlocation)
	  return Targetlocation
	elif (shared_coordinates_diffY <= 1+0.2 and shared_coordinates_diffY >= 1-0.2):
	  Targetlocation=[abs(sub_x1),0]
	  print ("Targetlocation",Targetlocation)
	  return Targetlocation
	else:
	  print "ERROR"
	  
	  
    def getLaneDimentions(self,wayTarget,neighbourWay,origin):
      #returnes the length of a lane 
	self.setOrigin(origin)
	shared_coordinates=[] 
	NotSharedLaneNodes=[]
	wayAllCoordinates = []
	nodes = self.getWayNodes(wayTarget)
	print ("nodes",nodes)
	nodes1 = self.getWayNodes(neighbourWay)
	for node in nodes:
	    x1=self.getLocalCoordinates(node)[0]
	    y1=self.getLocalCoordinates(node)[1]
	    wayAllCoordinates.append(x1)
	    wayAllCoordinates.append(y1)
	    for nodee in nodes1:
		x2=self.getLocalCoordinates(nodee)[0]
		y2=self.getLocalCoordinates(nodee)[1]
		if x1 == x2 and y1==y2 :
		  shared_coordinates.append(x1)
		  shared_coordinates.append(y1)
	print ("wayAllCoordinates",wayAllCoordinates)
	print ("shared_coordinates",shared_coordinates)
	#NotSharedLaneNodes =list((set(wayAllCoordinates).difference(shared_coordinates)))
	NotSharedLaneNodes = [item for item in wayAllCoordinates if item not in shared_coordinates]
	#print ("temp3",temp3)


	print ("NotSharedLaneNodes",NotSharedLaneNodes)
	sub_x1= shared_coordinates[0]-NotSharedLaneNodes[0]
	sub_y1= shared_coordinates[1]-NotSharedLaneNodes[1]
	shared_coordinates_diffX = abs (shared_coordinates[0]-shared_coordinates[2])
	shared_coordinates_diffY =abs (shared_coordinates[1]-shared_coordinates[3])
	if (shared_coordinates_diffX <= 1+0.2 and shared_coordinates_diffX >= 1-0.2):
	  lanedimention=[0,abs(sub_y1)]
	  print ("lane dimention",lanedimention)
	  return lanedimention
	elif (shared_coordinates_diffY <= 1+0.2 and shared_coordinates_diffY >= 1-0.2):
	  lanedimention=[abs(sub_x1),0]
	  print ("lanedimention",lanedimention)
	  return lanedimention
	else:
	  print "ERROR"

	  
    def getWayLength(self, way_name):
	#function receives way name and return length of way
	nodes_list = self.getWayNodes(way_name)
	point1 = self.getCoordinates(nodes_list[0])
	point2 = self.getCoordinates(nodes_list[1])
	point3 = self.getCoordinates(nodes_list[2])
	point4 = self.getCoordinates(nodes_list[3])
	point1 = utm.from_latlon(point1[0],point1[1])
	point2 = utm.from_latlon(point2[0],point2[1])
	point3 = utm.from_latlon(point3[0],point3[1])
	point4 = utm.from_latlon(point4[0],point4[1])
	distance1 = math.sqrt(math.pow((point2[0] - point3[0]),2) + math.pow((point2[1] - point3[1]),2)	)
	distance2 = math.sqrt(math.pow((point1[0] - point3[0]),2) + math.pow((point1[1] - point3[1]),2)	)
	distance3 = math.sqrt(math.pow((point1[0] - point2[0]),2) + math.pow((point1[1] - point2[1]),2)	)
	distance4 = math.sqrt(math.pow((point1[0] - point4[0]),2) + math.pow((point1[1] - point4[1]),2)	)
	length = max(distance1,distance2,distance3,distance4)
	return length

     
    