#!/usr/bin/env python

import rospy
import smach
import smach_ros
from smach import CBState

import robot_initialization1 
import stay_in_the_lane_lazy_right1
import stay_in_the_lane_lazy_left1
import stop
import global_planner1
import racking_detection_right1
import racking_detection_left1
import query1
import sensing1

#stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
#stay_in_the_lane_left = stay_in_the_lane_lazy_left1.stay_in_the_lane()
 
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['initialized','failed'])
def init_callback( userdata):
    init = robot_initialization1.robot_init ()
    if init.final_init() == True:
        print "Data is availble"
        return 'initialized'

    else:
        print "No data from sensors"
        return 'failed'
@smach.cb_interface(input_keys=[], output_keys=['position'], outcomes=['yes','no'])
def docking_callback (userdata):
    docking_area = raw_input("Am I in the docking area? 1 for true/ 0 for false ")  
    if docking_area =="1" :
        #global POSITION
    	userdata.position = "lane2"
        return 'yes'
    else :
            return 'no'


@smach.cb_interface(input_keys=['position'], output_keys=['position'], outcomes=['yes'])
def  update_position_callback (userdata): 
	#POSITION = "lane2" # lane 1 
	userdata.position =  "lane2"
	postion_in_update_position_callback = userdata.position
	print("position in update_position_callback",postion_in_update_position_callback)
	return 'yes'



@smach.cb_interface(input_keys=[], output_keys=['target_out'], outcomes=['yes','finished'])
def get_target_callback (userdata): 
	target =  raw_input("What is your target? racking 1 or 2") 
	userdata.target_out = target 
	if target == 'finished' :
	   return 'finished'
	else:
	   return 'yes'

@smach.cb_interface(input_keys=['target_in','position'], output_keys=['route'], outcomes=['yes'])
def global_planner_callback (userdata):
	planner = global_planner1.global_planner ()
    	print ("POSITION= ", userdata.position , "target", userdata.target_in)
	route = planner.lane_planner(userdata.position,userdata.target_in)
	print ("Route= ", route)
	userdata.route = route
        return 'yes' 

########################################################################### Moving and turning 
@smach.cb_interface(input_keys=['position'], output_keys=[], outcomes=['yes','no'])
def move_in_the_lane_callback (userdata):
    print('current position',userdata.position)
    if userdata.position == 'lane1':
      print("move inside lane1")

      stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
      stay_in_the_lane_right
      
    elif userdata.position == 'lane2':
      print("move inside lane2")
      stay_in_the_lane_left = stay_in_the_lane_lazy_left1.stay_in_the_lane()
      stay_in_the_lane_left
      #TODO replace question with detection
    corner = 0
    
    
    
    if corner== 1:
        return 'yes'
    else : 
        return 'no'

@smach.cb_interface(input_keys=['position'], output_keys=[], outcomes=['yes'])
def move_to_target_callback (userdata):
    if userdata.position == 'lane1':
      
      stay_in_the_lane_right = stay_in_the_lane_lazy_right1.stay_in_the_lane()
      stay_in_the_lane_right
      
    elif userdata.position == 'lane2':
      stay_in_the_lane_left = stay_in_the_lane_lazy_left1.stay_in_the_lane()
      stay_in_the_lane_left
      #stay_in_the_lane
      #TODO replace question with detection
      
    return 'yes'

@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def turn_callback (userdata):
    mov.rotate_left_90()
    return 'yes'


############################################################################ Detecting

@smach.cb_interface(input_keys=['position'], output_keys=['detected_line_length_right','detected_slope_right','detected_line_length_left','detected_slope_left' ], outcomes=['yes','no'])
def detect_feature_callback (userdata):
    line_right = racking_detection_right.line_length_right
    line_left = racking_detection_left.line_length_left
    userdata.detected_line_length_right = racking_detection_right.line_length_right
    userdata.detected_line_length_left = racking_detection_left.line_length_left
    userdata.detected_slope_right = racking_detection_right.detected_slope_right
    userdata.detected_slope_left = racking_detection_left.detected_slope_left
    
    if line_right == 0 or line_left == 0 :
       return 'no'
    else:
      return 'yes'

@smach.cb_interface(input_keys=[], output_keys=['detected_feature'], outcomes=['yes','no'])
def detect_target_callback (userdata):
    detect_target = raw_input("Do you see target ? (yes or no)")
    if detect_target == 'yes' :
        return 'yes'
    else : 
        return 'no'


############################################################################## Querying
@smach.cb_interface(input_keys=['position'], output_keys=['world_model_data','line_length_WM'], outcomes=['yes'])
def query_data_callback (userdata):
    userdata.world_model_data = 3
    query = query1.query()
    print("asdf",query.getWayNodes(userdata.position))
    #TODO change that with WM querying
    userdata.line_length_WM = 1.8
    return 'yes'


@smach.cb_interface(input_keys=[], output_keys=['world_model_data'], outcomes=['yes'])
def query_target_location_callback (userdata):
    userdata.world_model_data = 3
    return 'yes'



@smach.cb_interface(input_keys=['detected_line_length_right','detected_slope_right','line_length_WM','detected_line_length_left','detected_slope_left'], output_keys=[], outcomes=['yes','no'])
def compare_data_callback (userdata):
    #bullshit = raw_input("yolo ? (yes or no)")

    slope_right = 0.00000
    slope_left = 0.00000
    flag_racking_right = 0
    flag_racking_left = 0
    if (userdata.detected_line_length_right <= (userdata.line_length_WM + 0.25)) and (userdata.detected_line_length_right >= (userdata.line_length_WM - 0.25)):
	    	print ("I see a racking from the right side")
	    	flag_racking_right = 1
	    	slope_right = userdata.detected_slope_right
	    	#print ("slope",slope)
    else: 
	  #print("bestParameter1",bestParameter1)
	  #print("slope in smaller part of racking",slope)
	  if (userdata.detected_slope_right >= slope_right - 0.2) and (userdata.detected_slope_right <= slope_right + 0.2):
	      #print("bestParameter1",bestParameter1)
	      print ("I still see a racking from the right side")
	      flag_racking_right = 1
	      
    if (userdata.detected_line_length_left <= (userdata.line_length_WM + 0.25)) and (userdata.detected_line_length_left >= (userdata.line_length_WM - 0.25)):
	    	print ("I see a racking from the left side")
	    	flag_racking_left = 1
	    	slope_left = userdata.detected_slope_right
    else: 
	  if (userdata.detected_slope_left >= slope_left - 0.2) and (userdata.detected_slope_left <= slope_left + 0.2):
	      #print("bestParameter1",bestParameter1)
	      print ("I still see a racking from the left side")
	      flag_racking_left = 1
	      
	      
    if flag_racking_right == 1 or flag_racking_left == 1:
        return 'yes'
    else :
        return 'no'
@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes','no'])
def check_target_lane_callback (userdata):
    target_lane= raw_input("Is the the target in this lane ?  (yes or no) ") 
    if target_lane == 'yes':
        return 'yes'
    else:
        return 'no'    


@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def positioning_callback (userdata):
    return 'yes'


@smach.cb_interface(input_keys=[], output_keys=[], outcomes=['yes'])
def stop_callback (userdata):
    print ("I am stopping the robot")
    stop_robot = stop.stop()
    stop_robot
    return 'yes'
      




if __name__ == '__main__':
    rospy.init_node('state_machine')
    
    racking_detection_right = racking_detection_right1.Ransac() 
    racking_detection_left = racking_detection_left1.Ransac()
    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['outcome1','outcome2'])
    sm.userdata.Sm_Target = 0
    sm.userdata.Sm_in1 = 0
    sm.userdata.Sm_in2 = 0

    #sm.userdata.rspeed = 1.2
       # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('state_machine', sm, '/SM_statemachine')
    sis.start()
    # Open the container
    with sm:
    # Add states to the container
        smach.StateMachine.add('INITIALIZATION', CBState(init_callback), {'initialized': 'DOCKING', 'failed':'INITIALIZATION'})
        smach.StateMachine.add('DOCKING', CBState(docking_callback), {'yes': 'UPDATE_POSITION', 'no':'outcome2'})
        smach.StateMachine.add('UPDATE_POSITION', CBState(update_position_callback), {'yes': 'GET_TARGET'})
        smach.StateMachine.add('GET_TARGET', CBState(get_target_callback), {'yes': 'GLOBAL_PLANNER','finished': 'outcome1'},remapping={'target_out':'Sm_Target'})
        smach.StateMachine.add('GLOBAL_PLANNER', CBState(global_planner_callback), {'yes': 'QUERY_DATA'},remapping={'target_in':'Sm_Target'})
        smach.StateMachine.add('QUERY_DATA', CBState(query_data_callback),{'yes': 'MOVE_IN_THE_LANE'})
        smach.StateMachine.add('MOVE_IN_THE_LANE', CBState(move_in_the_lane_callback),{'no':'DETECT_FEATURE','yes':'TURN'})
        smach.StateMachine.add('DETECT_FEATURE', CBState(detect_feature_callback),{'no': 'COMPARE_DATA','yes':'COMPARE_DATA'})
        smach.StateMachine.add('TURN', CBState(turn_callback), {'yes': 'UPDATE_POSITION_3'})
        smach.StateMachine.add('UPDATE_POSITION_3', CBState(update_position_callback),{'yes': 'CHECK_TARGET_LANE'})
        smach.StateMachine.add('CHECK_TARGET_LANE', CBState(check_target_lane_callback),{'no': 'GLOBAL_PLANNER','yes':'QUERY_TARGET_LOCATION'})
        smach.StateMachine.add('QUERY_TARGET_LOCATION', CBState(query_target_location_callback),{'yes': 'DETECT_TARGET'})
        smach.StateMachine.add('MOVE_TO_TARGET', CBState(move_to_target_callback),{'yes': 'DETECT_TARGET'})
        smach.StateMachine.add('DETECT_TARGET', CBState(detect_target_callback),{'no': 'MOVE_TO_TARGET','yes':'POSITIONING'})
        smach.StateMachine.add('POSITIONING', CBState(positioning_callback),{'yes':'STOP'})
        smach.StateMachine.add('UPDATE_POSITION_4', CBState(update_position_callback),{'yes':'GET_TARGET'})
        smach.StateMachine.add('COMPARE_DATA', CBState(compare_data_callback),{'no':'MOVE_IN_THE_LANE','yes': 'UPDATE_POSITION_2'})
        smach.StateMachine.add('UPDATE_POSITION_2', CBState(update_position_callback),{'yes': 'MOVE_IN_THE_LANE'})
        smach.StateMachine.add('STOP', CBState(stop_callback),{'yes': 'UPDATE_POSITION_4'})

    sis = smach_ros.IntrospectionServer('state_machine', sm, '/SM_statemachine')
    sis.start()
    # Execute SMACH plan 
    outcome = sm.execute() 
    #rospy.spin()
    sis.stop()
